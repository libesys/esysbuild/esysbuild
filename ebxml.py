## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

class Writer:
    def __init__(self):
        self.m_open_element=False
        self.m_break_attrib=False
        self.m_is_content=False
        self.m_stack_el=[]
        self.m_fout=None
        self.m_filename=None
        self.m_level=0
        self.m_ident="    "
    def SetOpenElement(self, open_element):
        self.m_open_element=open_element
    def GetOpenElement(self):
        return self.m_open_element
    def SetBreakAttrib(self, break_attrib):
        self.m_break_attrib=break_attrib
    def GetBreakAttrib(self):
        return self.m_break_attrib
    def SetIsContent(self, is_content):
        self.m_is_content=is_content
    def GetIsContent(self):
        return self.m_is_content
    def SetFileName(self, filename):
        self.m_filename=filename
    def SetLevel(self, level):
        self.m_level
    def GetLevel(self):
        return self.m_level
    def SetIdent(self, ident):
        self.m_ident=ident
    def GetIdent(self):
        return self.m_ident
    def GetStackElSize(self):
        return len(self.m_stack_el)
    def Write(self, text):
        assert self.m_fout<>None
        self.m_fout.write(text)
    def PushEl(self, name):
        self.m_stack_el.append(name)
    def GetTopEp(self):
        return self.m_stack_el[-1]
    def PopEl(self):
        self.m_stack_el=self.m_stack_el[:-1]
    def StartDocument(self, encoding="UTF-8", standalone=None):
        self.m_fout=open(self.m_filename, "wt")
        s="<?xml version=\"1.0\" encoding=\"%s\"" % encoding
        if standalone<>None:
            if standalone==True:
                s+=" standalone=\"yes\""
            else:
                s+=" standalone=\"no\""
        s+="?>\n"
        self.m_fout.write(s)
    def EndDocument(self):
        assert len(self.m_stack_el)==0
        self.m_fout.write("\n")
        self.m_fout.close()
    def StartElement(self, name):
        self.CloseStartElement()
        self.ConditionalLineBreak(not self.GetIsContent(), self.GetStackSize()-1)
        self.m_fout.write("<%s" % name)
        self.PushEl(name)
        self.SetOpenElement(True)
        self.SetBreakAttrib(False)
    def EndElement(self):
        assert self.GetStackElSize()<>0

        if self.GetOpenElement()==True:
            self.m_fout.write("/>")
        else:
            self.ConditionalLineBreak(not self.GetIsContent(), self.GetStackSize()-1)
            self.SetIsContent(False)
            self.m_fout.write("</%s>" % self.GetTopEp())
        self.PopEl()
        self.SetOpenElement(False)
    def Element(self, name, value=None):
        if value==None:
            self.CloseStartElement()
            self.ConditionalLineBreak(not self.GetIsContent(), self.GetStackElSize())
            self.m_fout.write("<%s/>" % name)
        else:
            self.StartElement(name)
            self.Content(value)
            self.EndElement()
    def Comment(self, comment):
        self.CloseStartElement()
        self.ConditionalLineBreak(not self.GetIsContent(), self.GetStackElSize())
        self.m_fout.write("<!-- %s -->" % comment)
    def CData(self, data):
        self.PreContent()
        self.m_fout.write("<![CDATA[%s]]>" % data)
    def DocType(self, doc_type):
        self.CloseStartElement()
        self.ConditionalLineBreak(not self.GetIsContent(), self.GetStackElSize())
        self.m_fout.write("<!DOCTYPE %s>" % doc_type)
    def ProcessingInstruction(self, target, data):
        self.CloseStartElement()
        self.ConditionalLineBreak(not self.GetIsContent(), self.GetStackElSize())
        self.m_fout.write("<?%s %s?>" % (target, data))
    def FragmentFile(self, filename):
        self.CloseStartElement()
        fin=open(filename, "rb")
        data=fin.read()
        fin.close()
        self.m_fout.write(data)
    def ConditionalLineBreak(self, condition, indent):
        if condition==False:
            return
        self.m_fout.write("\n")
        for i in range(indent+self.GetLevel()):
            self.m_fout.write(self.GetIdent())
    def PreAttribute(self):
        assert self.GetOpenElement()
        self.ConditionalLineBreak(self.GetBreakAttrib(), self.GetStackElSize())
        if not self.GetBreakAttrib():
            self.Write(" ")
    def PreContent(self):
        self.CloseStartElement()
        self.SetIsContent(True)
    def CloseStartElement(self):
        if self.GetOpenElement():
            self.ConditionalLineBreak(self.GetBreakAttrib(), self.GetStackElSize())
            self.Write(">")
            self.SetOpenElement(False)
    def SafeAttribute(self, value):
        return value
    def SafeContent(self, content):
        return content
    def Attribute(self, name, value):
        self.PreAttribute()
        self.Write("%s=\"%s\"" % (name, self.SafeAttribute(value)))
    def Content(self, content):
        self.PreContent()
        self.Write(self.SafeContent(content))




