import os


class SrcTreeItem:
    CFLAGS=0
    CXXFLAGS=1
    LDFLAGS=2
    FILE=0
    FOLDER=1
    def __init__(self):
        self.m_parent=None
        self.m_children=[]
        self.m_next=None
        self.m_prev=None
        self.m_flags_txt={}
        self.m_name=None
        self.m_type=None
        self.m_path=None
        self.m_tree=None
    def SetParent(self, parent):
        self.m_parent=parent
    def GetParent(self):
        return self.m_parent
    def AddChild(self, child):
        self.m_children.append(child)
        child.SetParent(self)
    def GetChildren(self):
        return self.m_children
    def GetChildrenCount(self):
        return len(self.m_children)
    def GetChild(self, idx):
        if idx>=self.GetChildrenCount():
            return None
        return self.m_children[idx]
    def SetNext(self, next):
        self.m_next=next
    def GetNext(self):
        return self.m_next
    def SetPrev(self, prev):
        self.m_prev=prev
    def GetPrev(self):
        return self.m_prev
    def SetFlags(self, kind, flags):
        assert self.m_flags_txt.has_key(kind)==False

        self.m_flags_txt[kind]=flags
    def GetFlags(self, kind):
        if self.m_flags_txt.has_key(kind)==False:
            return None
        return self.m_flags_txt[kind]
    def SetName(self, name):
        self.m_name=name
    def GetName(self):
        return self.m_name
    def SetType(self, typ):
        self.m_type=typ
    def GetType(self):
        return self.m_type
    def SetPath(self, path):
        self.m_path=path
    def GetPath(self, path):
        return self.m_path
    def SetTree(self, tree):
        self.m_tree=tree
    def GetTree(self):
        if self.m_tree<>None:
            return self.m_tree
        if self.GetParent()==None:
            return None
        tree=self.GetParent().GetTree()
        self.SetTree(tree)
        return tree
    def AddFolder(self, name):
        folder=Folder(name)
        self.AddChild(folder)
        return folder
    def AddFile(self, name):
        file=File(name)
        self.AddChild(file)
        return file

class Folder(SrcTreeItem):
    def __init__(self, name):
        SrcTreeItem.__init__(self)
        self.SetName(name)
        self.SetPath(self.FOLDER)

class File(SrcTreeItem):
    def __init__(self, name):
        SrcTreeItem.__init__(self)
        self.SetName(name)
        self.SetPath(self.FILE)

