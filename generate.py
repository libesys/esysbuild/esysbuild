#!/usr/bin/env python3

## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os

class Generate:
    def __init__(self, file_name):
        self.m_file_name=file_name
        self.m_state=0
        self.m_fout=None
        
    def Do(self):
        fin=open(self.m_file_name, "rt")
        lines=fin.readlines()
        fin.close()
        line_count=1
        for line in lines:
            if self.m_state==0:
                if line.find("##XESYSBUILD")!=-1:
                    line=line.replace("##XESYSBUILD", "")
                    if line.find("file")!=-1:
                        i=line.find("name")
                        if i==-1:
                            print("ERROR on line %i: a name should be provided")
                            return -1
                        line=line[i+4:]
                        line=line.replace(">", "")
                        line=line.replace("\"", "")
                        line=line.replace("=", "")
                        line=line.replace("\n", "")
                        name=line.strip()
                        name=name.replace(".", os.sep)
                        if name.rfind(".py")==-1:
                            name+=".py"
                        print("Writing file %s ..." % name)
                        self.m_fout=open(name,"wt")
                        self.m_state=1
            elif self.m_state==1:
                if line.find("##XESYSBUILD")==-1:
                    self.m_fout.write(line)
                else:
                    line=line.replace("##XESYSBUILD", "")
                    if line.find("/file")!=-1:
                        self.m_fout.close()
                        self.m_fout=None
                        self.m_state=0
                        print("Writing file %s done." % name)
                    elif line.find("content")!=-1:
                        self.m_state=2
            elif self.m_state==2:
                if line.find("##XESYSBUILD")==-1:
                    self.m_fout.write(line[1:]) # remove the first character
                else:
                    line=line.replace("##XESYSBUILD", "")
                    if line.find("/content")!=-1:
                        self.m_state=1
            line_count+=1
        return 0
    
def main():
    gen=Generate("main.py")
    gen.Do()

if __name__ == "__main__":
    main()
