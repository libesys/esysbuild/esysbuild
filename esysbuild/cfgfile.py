## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
from esysbuild.dynmod import *

class CfgFile(DynMod):
    ROOT=0
    BUILD=1    
    def __init__(self, typ=None, **kwds):
        super().__init__(**kwds)
        if typ==None:
            self.m_type=CfgFile.ROOT
        else:
            self.m_type=typ
        self.m_in_dev_folder=False
        self.m_esysbuild_src_dir="esysbuild"    #default in subfolder of CfgFile, always relative path
        self.m_folder_path=None
        self.m_file_name=None
        self.m_build_name=None
    def SetPath(self, path):
        super().SetPath(path)
        folder_path=os.path.split(path)[0]
        self.SetFolderPath(folder_path)
    def SetType(self, typ):
        self.m_type=typ
    def GetType(self):
        return self.m_type
    def SetInDevFolder(self, in_dev_folder):
        self.m_in_dev_folder=in_dev_folder
    def GetInDevFolder(self):
        return self.m_in_dev_folder
    def SetESysBuildSrcDir(self, esysbuild_src_dir):
        self.m_esysbuild_src_dir=esysbuild_src_dir
    def GetESysBuildSrcDir(self):
        return self.m_esysbuild_src_dir
    def SetFolderPath(self, folder_path):
        self.m_folder_path=folder_path
    def GetFolderPath(self):
        return self.m_folder_path
    def SetFileName(self, filename):
        self.m_file_name=filename
    def GetFileName(self):
        return self.m_file_name
    def SetBuildName(self, build_name):
        self.m_build_name=build_name
    def GetBuildName(self):
        return self.m_build_name
    def Write(self, folder_path, filename="config.py", class_name="TheCfgFile"):
        path=os.path.join(folder_path, filename)
        self.SetFolderPath(folder_path)
        self.SetFileName(filename)
        fout=open(path, "wt")
        
        fout.write("def GetCfgFile(cfg_file, CfgFile):\n")
        fout.write("    cfg_file.SetType(")
        if self.GetType()==CfgFile.ROOT:
            fout.write("CfgFile.ROOT")
        elif self.GetType()==CfgFile.BUILD:
            fout.write("CfgFile.BUILD")
        else:
            fout.close()
            return -1
        fout.write(")\n")
        fout.write("    cfg_file.SetInDevFolder(%s)\n" % self.GetInDevFolder())
        fout.write("    cfg_file.SetESysBuildSrcDir(\"%s\")\n" % self.GetESysBuildSrcDir())
        fout.write("    cfg_file.SetFolderPath(\"%s\")\n" % self.GetFolderPath())
        fout.write("    cfg_file.SetFileName(\"%s\")\n" % self.GetFileName())
        if self.GetType()==CfgFile.BUILD:
            fout.write("    cfg_file.SetBuildName(\"%s\")\n" % self.GetBuildName()) 
        fout.write("    return cfg_file\n")
        fout.close()
        return 0
    
        fout.write("from esysbuild.cfgfile import *\n\n")
        fout.write("class "+class_name+"(CfgFile):\n")
        fout.write("    def __init__(self):\n")
        fout.write("        super().__init__()\n")
        fout.write("        self.SetType(")
        if self.GetType()==CfgFile.ROOT:
            fout.write("CfgFile.ROOT")
        elif self.GetType()==CfgFile.BUILD:
            fout.write("CfgFile.BUILD")
        else:
            fout.close()
            return -1
        fout.write(")\n")        
        fout.write("        self.SetInDevFolder(%s)\n" % self.GetInDevFolder())
        fout.write("        self.SetESysBuildSrcDir(\"%s\")\n" % self.GetESysBuildSrcDir())
        fout.write("        self.SetFolderPath(\"%s\")\n" % self.GetFolderPath())
        fout.write("        self.SetFileName(\"%s\")\n" % self.GetFileName())
        if self.GetType()==CfgFile.BUILD:
            fout.write("        self.SetBuildName(\"%s\")\n" % self.GetBuildName())
        fout.write("\n")
        fout.write("def %s():\n" % "GetCfgFile")
        fout.write("    return %s()\n" % class_name)
        fout.close()
        return 0
    def CopyToBuildType(self, path, build_name):
        if self.GetType()!=CfgFile.ROOT:
            return None        
        cfg_file=CfgFile()
        cfg_file.SetType(CfgFile.BUILD)
        cfg_file.SetFolderPath(path)
        cfg_file.SetBuildName(build_name)
        cfg_file.SetInDevFolder(self.GetInDevFolder())

        orig_src_dir=os.path.join(self.GetFolderPath(), self.GetESysBuildSrcDir())
        orig_src_dir=os.path.normpath(orig_src_dir)        
        new_src_dir=os.path.relpath(orig_src_dir, path)
        cfg_file.SetESysBuildSrcDir(new_src_dir)        
        return cfg_file
        
    @staticmethod        
    def Load(path, name="config"):
        return DynMod.Load(path, "config", "GetCfgFile", [CfgFile(), CfgFile])
    
