## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import subprocess
from esysbuild.dynmod import *

class CompilerFile(DynMod):    
    def __init__(self, name=None, version=None):
        super().__init__()
        self.m_name=name
        self.m_version=version
        self.m_cc_rel_path=None
        self.m_cxx_rel_path=None
        self.m_ld_rel_path=None
        self.m_ar_rel_path=None
        self.m_nm_rel_path=None
        self.m_ran_lib_rel_path=None
        self.m_obj_copy_rel_path=None
        self.m_obj_dump_rel_path=None
        self.m_size_rel_path=None
        self.m_prog_rel_path_dict={}
        self.m_alt_names=set()
    def AddAltName(self, alt_name):
        self.m_alt_names.add(alt_name)
    def GetAltNameList(self):
        return list(self.m_alt_names)
    def SetName(self, name):
        self.m_name=name
    def GetName(self):
        return self.m_name
    def SetCCRelPath(self, cc_rel_path):
        self.m_prog_rel_path_dict["CC"]=cc_rel_path
        self.m_cc_rel_path=cc_rel_path
    def GetCCRelPath(self):
        return self.m_cc_rel_path
    def SetCXXRelPath(self, cxx_rel_path):
        self.m_prog_rel_path_dict["CXX"]=cxx_rel_path
        self.m_cxx_rel_path=cxx_rel_path
    def GetCXXRelPath(self):
        return self.m_cxx_rel_path
    def SetLDRelPath(self, ld_rel_path):
        self.m_prog_rel_path_dict["LD"]=ld_rel_path
        self.m_ld_rel_path=ld_rel_path
    def GetLDRelPath(self):
        return self.m_ld_rel_path
    def SetARRelPath(self, ar_rel_path):
        self.m_prog_rel_path_dict["AR"]=ar_rel_path
        self.m_ar_rel_path=ar_rel_path
    def GetARRelPath(self):
        return self.m_ar_rel_path
    def SetNMRelPath(self, nm_rel_path):
        self.m_prog_rel_path_dict["NM"]=nm_rel_path
        self.m_nm_rel_path=nm_rel_path
    def GetNMRelPath(self):
        return self.m_nm_rel_path
    def SetRanLibRelPath(self, ran_lib_rel_path):
        self.m_prog_rel_path_dict["RANLIB"]=ran_lib_rel_path
        self.m_ran_lib_rel_path=ran_lib_rel_path
    def GetRanLibRelPath(self):
        return self.m_ran_lib_rel_path
    def SetObjCopyRelPath(self, obj_copy_rel_path):
        self.m_prog_rel_path_dict["OBJCOPY"]=obj_copy_rel_path
        self.m_obj_copy_lib_rel_path=obj_copy_rel_path
    def GetObjCopyRelPath(self):
        return self.m_obj_copy_rel_path
    def SetObjDumpRelPath(self, obj_dump_rel_path):
        self.m_prog_rel_path_dict["OBJDUMP"]=obj_dump_rel_path
        self.m_obj_dump_lib_rel_path=obj_dump_rel_path
    def GetObjDumpRelPath(self):
        return self.m_obj_dump_rel_path
    def SetSizeRelPath(self, size_rel_path):
        self.m_prog_rel_path_dict["SIZE"]=size_rel_path
        self.m_size_lib_rel_path=size_rel_path
    def GetSizeRelPath(self):
        return self.m_size_rel_path
    def SetVersion(self, version):
        self.m_version=version        
    def GetVersion(self):
        return self.m_version
    def GetProgRelPath(self, prog):
        prog=prog.upper()
        if prog in self.m_prog_rel_path_dict:
            return self.m_prog_rel_path_dict[prog]
        return None
    def GetProgPath(self, prog):
        rel_path=self.GetProgRelPath(prog)
        if rel_path==None:
            return None
        path=os.path.split(self.GetPath())[0]
        return os.path.join(path, rel_path)
    def GetToolchainPath(self):
        if self.GetPath()==None:
            return None
        path=os.path.split(self.GetPath())[0]
        return path
    def GetFamilyName(self):
        name=self.GetName()
        i=name.find("-")
        if i!=-1:
            name=name[:i]
        return name
     
    @staticmethod        
    def Load(path, name="compiler.cfg"):
        return DynMod.Load(path, "compiler.cfg", "GetCompilerFile")

class DefaultCompiler(CompilerFile):
    def __init__(self):
        super().__init__("gcc")                
        self.AddAltName("default")
        #self.SetVersion("4.9-2015q3")
        self.SetCXXRelPath("g++")
        self.SetCCRelPath("gcc")
        self.SetLDRelPath("ld")
        self.SetARRelPath("ar")
        self.SetNMRelPath("nm")
        self.SetRanLibRelPath("ranlib")
        self.SetObjCopyRelPath("objcopy")
        self.SetObjDumpRelPath("objdump")
        self.SetSizeRelPath("size")
        version=self.Cmd("gcc -dumpversion")
        self.SetVersion(version)        
    def GetProgPath(self, prog):
        rel_path=self.GetProgRelPath(prog)
        return rel_path
    def Cmd(self, cmd):
        DEVNULL = open(os.devnull, 'wb')
        try:
            result=subprocess.check_output(cmd,  stderr=DEVNULL, shell=True)
        except subprocess.CalledProcessError:
            result=None            
        DEVNULL.close()
        if result==None:
            return None
        result=result.decode("utf-8")
        result=result.replace("\n","")
        return result
