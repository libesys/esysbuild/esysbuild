## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import glob
from esysbuild.compilerfile import *

class CompilerMngr:
    def __init__(self):
        self.m_base_dir=None
        self.m_verbose_obj=None
        self.m_compiler_dict={}
        self.m_compiler_list=[]
        self.m_alt_name_dict={}
        self.m_dft_compiler=DefaultCompiler()
        if self.m_dft_compiler.GetVersion()==None:
            self.m_dft_compiler=None
        else:
            self.Add(self.m_dft_compiler)
    def AddAltName(self, alt_name, name):
        if type(alt_name)==list:
            for alt in alt_name:
                self.m_alt_name_dict[alt]=name
        else:
            self.m_alt_name_dict[alt_name]=name
    def FindNameByAltName(self, alt_name):
        if alt_name in self.m_alt_name_dict:
            return self.m_alt_name_dict[alt_name]
        return None            
    def SetVerboseObj(self, verbose_obj):
        self.m_verbose_obj=verbose_obj
    def Verbose(self, verbose, txt, start=False, end=False):
        if self.m_verbose_obj==None:
            return
        self.m_verbose_obj.Verbose(verbose, txt, start, end)
    def SetBaseDir(self, base_dir):
        self.m_base_dir=base_dir
    def GetBaseDir(self):
        return self.m_base_dir
    def Add(self, cxx):
        name=cxx.GetName()
        alt_names=cxx.GetAltNameList()
        self.AddAltName(alt_names, name)
        version=cxx.GetVersion()
        if (name in self.m_compiler_dict)==False:
            self.m_compiler_dict[name]={}
        if (version in self.m_compiler_dict[name])==True:
            self.Verbose(0, "WARNING: same compiler version already found %s %s" % (name, version))
        self.m_compiler_dict[name][version]=cxx
        self.m_compiler_list.append(cxx)
    def Count(self):
        return len(self.m_compiler_list)
    def Get(self, idx):
        if idx>=self.Count():
            return None
        return self.m_compiler_list[idx]
    def GetList(self):
        return self.m_compiler_list
    def Find(self, name, version=None):
        if name in self.m_alt_name_dict:
            name=self.m_alt_name_dict[name]
        if (name in self.m_compiler_dict)==False:
            return None
        if version!=None:
            if (version in self.m_compiler_dict[name])==True:
                return self.m_compiler_dict[name][version]
            if len(self.m_compiler_dict[name])==1:
                l=list(self.m_compiler_dict[name].values())
                return l[0]
            else:                
                l=list(self.m_compiler_dict[name].values())
                for compiler in l:
                    compiler_version=compiler.GetVersion()
                    if compiler_version.find(version)!=-1:
                        return compiler                    
                return None
        else:
            l=list(self.m_compiler_dict[name].values())
            version=""
            result=None
            for comp in l:
                if comp.GetVersion()>version:
                    version=comp.GetVersion()
                    result=comp            
            return result
    def LoadAll(self):
        self.Verbose(2, "CompilerMngr.LoadAll")
        compilers=glob.glob(os.path.join(self.m_base_dir, "*"))
        for compiler in compilers:            
            self.Verbose(2, compiler)
            path=os.path.join(compiler, "compiler.cfg.py")
            result, cxx_file=CompilerFile.Load(path)
            if result<0:
                self.Verbose(0, "ERROR: couldn't load compiler info at %s" % compiler)
            else:
                self.Add(cxx_file)
        return 0
