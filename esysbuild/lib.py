## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import sys
from esysbuild.groupmngrbase import *

class Lib(GroupMngrBase):
    LINK_INSTALLED = 0
    LINK_LIBTOOL = 1
    LINK_PKG = 2
    LINK_DFLT = 3
    def __init__(self, name, ext=False, location=None, **kwds):
        super().__init__(**kwds)
        self.m_name=name
        self.m_is_external=ext
        self.m_headers_only=False
        self.m_repo=None
        self.m_link_cfg={}
        self.m_dependencies_list=[]
        self.m_dependencies_dict={}        
        if location==None:
            if ext==False:
                self.m_location="src/%s" % name
            else:
                self.m_location="extlib/%s" % name
        else:
            self.m_location=location
    def GetName(self):
        return self.m_name
    def SetName(self, name):
        self.m_name=name
    def GetLocation(self):
        return self.m_location
    def SetLocation(self, location):
        self.m_location=location
    def SetIsExternal(self, is_external):
        self.m_is_external=is_external
    def IsExternal(self):
        return self.m_is_external     
    def SetHeadersOnly(self, headers_only=True):
        self.m_headers_only=headers_only
    def GetHeadersOnly(self):
        return self.m_headers_only
    def SetRepo(self, repo):
        self.m_repo=repo
    def GetRepo(self):
        return self.m_repo
    def SetLinkCfg(self, cfg_type, cfg_link):
        self.m_link_cfg[cfg_type]=cfg_link
    def GetLinkCfg(self, cfg_type):
        if cfg_type in self.m_link_cfg:
            return self.m_link_cfg[cfg_type]
        return None
    def AddDependency(self, lib):
        if (lib in self.m_dependencies_dict)==True:
            return
        self.m_dependencies_dict[lib]=True
        self.m_dependencies_list.append(lib)
    def AddDependencies(self, libs):
        if type(libs)==str:
            self.AddDependency(libs)
        elif type(libs)==list:
            for lib in libs:
                self.AddDependency(lib)
    def GetDependenciesList(self):
        return self.m_dependencies_list
    def GetDependenciesDict(self):
        return self.m_dependencies_dict

            
    
        
    
