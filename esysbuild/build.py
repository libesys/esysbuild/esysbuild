## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
from esysbuild.dynmod import *
from esysbuild.groupmngrbase import *

class Build(DynMod, GroupMngrBase):
    def __init__(self, name=None, **kwds):
        super().__init__(**kwds)
        self.m_name=name
        self.m_full_name=None
        self.m_default=False
        self.m_board_name=None
        self.m_board=None
        self.m_cross_compile=False
        self.m_c_flags=None
        self.m_cxx_flags=None
        self.m_build_name_list=[]   # List of all libraries or executables part of this build
        self.m_build_name_dict={}   # List of all libraries or executables part of this build
        self.m_compiler=None
        self.m_static_build=None    # Not set
        self.m_shared_build=None    # Not set
        self.m_arch=None
    def SetName(self, name):
        self.m_name=name
    def GetName(self):
        return self.m_name
    def SetFullName(self, full_name):
        self.m_full_name=full_name
    def GetFullName(self):
        return self.m_full_name
    def SetDefault(self, default=True):
        self.m_default=default
    def GetDefault(self):
        return self.m_default
    def SetBoardName(self, board_name):
        self.m_board_name=board_name
    def GetBoardName(self):
        return self.m_board_name
    def SetBoard(self, board):
        self.m_board=board
    def GetBoard(self):
        return self.m_board
    def SetCrossCompile(self, cross_compile=True):
        self.m_cross_compile=cross_compile
    def GetCrossCompile(self):
        if self.m_board==None:
            return self.m_cross_compile
        return self.m_board.GetCrossCompile()
    def GetCFlags(self):
        return self.m_c_flags 
    def SetCFlags(self, c_flags):
        self.m_c_flags=c_flags
    def GetCXXFlags(self):
        return self.m_cxx_flags 
    def SetCXXFlags(self, cxx_flags):
        self.m_cxx_flags=cxx_flags
    def AddBuild(self, build):
        if (build in self.m_build_name_dict)==True:
            return
        self.m_build_name_dict[build]=True
        self.m_build_name_list.append(build)
    def AddBuilds(self, builds):
        if type(builds)==str:
            self.AddBuild(builds)
        elif type(builds)==list:
            for build in builds:
                self.AddBuild(build)
    def GetBuildNameList(self):
        return self.m_build_name_list
    def GetBuildNameDict(self):
        return self.m_build_name_dict
    def SetCompiler(self, compiler):
        self.m_compiler=compiler
    def GetCompiler(self):
        return self.m_compiler
    def StaticOnly(self):
        self.m_static_build=True
        self.m_shared_build=False
    def SharedOnly(self):
        self.m_static_build=False
        self.m_shared_build=True
    def Static(self):
        self.m_static_build=True
    def Shared(self):
        self.m_shared_build=True
    def IsStatic(self):
        return self.m_static_build
    def IsShared(self):
        return self.m_shared_build
    def SetArch(self, arch):
        self.m_arch=arch
    def GetArch(self):
        return self.m_arch
        
    @staticmethod        
    def Load(path):
        name=os.path.split(path)[1]
        return DynMod.Load(path, name, "GetBuild")
    