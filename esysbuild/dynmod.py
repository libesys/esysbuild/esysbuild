import os
import sys
import importlib.util
from esysbuild.dynmod import *

class DynMod(object):
    FILE_NO_EXISTING=-1
    SYNTAX_ERROR=-2
    GET_FCT_NOT_FOUND=-3
    def __init__(self, **kwds):
        super().__init__(**kwds)
        self.m_path=None
        self.m_module=None
        self.m_module_name=None
    def SetPath(self, path):
        self.m_path=path
    def GetPath(self):
        return self.m_path
    def SetModule(self, module):
        self.m_module=module
    def GetModule(self):
        return self.m_module
    def SetModuleName(self, module_name):
        self.m_module_name= module_name
    def GetModuleName(self):
        return self.m_module_name
    @staticmethod
    def LoadModulePy34(path, mod_name):
        import importlib.machinery
        try:
            module = importlib.machinery.SourceFileLoader(mod_name,path).load_module()
        except SyntaxError as ex:
            print("ERROR: config file %s" % path)
            print("    %s:" % ex)
            print("    %s" % ex.text.lstrip()) 
            return [DynMod.SYNTAX_ERROR,None]
        return [0, module]
    @staticmethod
    def LoadModulePy35(path, mod_name):
        spec = importlib.util.spec_from_file_location(mod_name, path)
        module = importlib.util.module_from_spec(spec)
        try:
            spec.loader.exec_module(module)
        except SyntaxError as ex:
            print("ERROR: config file %s" % path)
            print("    %s:" % ex)
            print("    %s" % ex.text.lstrip()) 
            return [DynMod.SYNTAX_ERROR,None]
        return [0, module]
    @staticmethod
    def LoadModule(path, mod_name):
        if sys.version_info.minor==4:
            return DynMod.LoadModulePy34(path, mod_name)
        elif sys.version_info.minor>=5:
            return DynMod.LoadModulePy35(path, mod_name)    
    @staticmethod        
    def Load(path, mod_name, fct_name, fct_params=None):
        if os.path.exists(path)==False:
            return [DynMod.FILE_NO_EXISTING,None]        
        result, module=DynMod.LoadModule(path, mod_name)
        if result<0:
            return [result, module]
        if fct_name in module.__dict__:
            try:
                if fct_params==None:
                    dyn_mod_file=module.__dict__[fct_name]()
                else:                
                    dyn_mod_file=module.__dict__[fct_name](*fct_params)
            except TypeError as ex:
                print("ERROR: config file %s" % path)
                print("    %s:" % ex)
                return [DynMod.SYNTAX_ERROR,None]
            except SyntaxError as ex:
                print("ERROR: config file %s" % path)
                print("    %s:" % ex)
                print("    %s" % ex.text.lstrip()) 
                return [DynMod.SYNTAX_ERROR,None]
            dyn_mod_file.SetModule(module)
            dyn_mod_file.SetPath(path)
            return [0,dyn_mod_file]
        else:
            return [DynMod.GET_FCT_NOT_FOUND,None]
        
