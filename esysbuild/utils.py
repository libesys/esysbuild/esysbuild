import os
import platform 
import subprocess
import stat

def IsCygwin():
    return (platform.system().lower().find("cygwin") != -1)

def IsMSys():
    return (platform.system().lower().find("msys") != -1)

def IsWindows():
    return (platform.system().lower().find("win") != -1) and (platform.system().lower().find("darwin") == -1)

def IsShellScript():
    return not IsWindows()

def IsBatScript():
    return IsWindows()

def MakeExecutable(file):   
    if (IsCygwin()==True) or (IsMSys()==True):
        cmd="setfacl -s u::rwx,g::r-x,o:r-x %s" % file
        result=subprocess.call(cmd, shell=True)        
    else:
        os.chmod(file,stat.S_IRWXU+stat.S_IRWXG+stat.S_IRWXO)