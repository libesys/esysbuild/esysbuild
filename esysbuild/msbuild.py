import os
import sys
import subprocess


class MSBuild:
    X86_32 = 0
    X86_64 = 1

    MSVC_VAR_PATH = {
        "2017": "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Auxiliary\\Build",
        "2019": "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\VC\\Auxiliary\\Build",
        "2022": "C:\\Program Files\\Microsoft Visual Studio\\2022\\Community\\VC\\Auxiliary\\Build",
    }

    TOOLSET_VERSION = {"2017": "15.0", "2019": "16.0", "2022": "17.0"}

    PLATFORM_TOOLSET = {"2017": "v141", "2019": "v142", "2022": "v143"}

    CMD_DETAILS = {
        X86_32: ["vcvars32.bat", "amd64_x86"],
        X86_64: ["vcvars64.bat", "amd64_x86"],
    }

    PLATFORM_NAMES = {
        X86_32: "Win32",
        X86_64: "x64",
    }

    def __init__(self):
        self.m_build_x86_32 = False
        self.m_build_x86_64 = True
        self.m_targets = None
        self.m_version = "2022"
        self.m_sln_path = None
        self.m_vcxproj_path = None
        self.m_add_targets = False
        self.m_call_esyssdkcli = True
        self.m_platform_toolset = None
        self.m_display_env = False
        self.m_platform_names = MSBuild.PLATFORM_NAMES
        self.m_build_no_dependencies = False
        self.m_rebuild = False
        self.m_env = None
        self.m_rel_output_folder = None
        self.m_base_folder = None

    def set_base_folder(self, base_folder):
        self.m_base_folder = base_folder

    def get_base_folder(self):
        return self.m_base_folder
    
    def clear_env(self):
        self.m_env = None

    def get_env(self):
        return self.m_env

    def set_env(self, env):
        self.m_env = env

    def add_env_var(self, name, value):
        if self.m_env is None:
            self.m_env = {}
        self.m_env[name] = value

    def set_rebuild(self, rebuild):
        self.m_rebuild = rebuild

    def get_rebuild(self):
        return self.m_rebuild

    def set_build_no_dependencies(self, build_no_dependencies):
        self.m_build_no_dependencies = build_no_dependencies

    def get_build_no_dependencies(self):
        return self.m_build_no_dependencies

    def set_platform_name(self, platform, name):
        self.m_platform_names[platform] = name

    def get_platform_name(self, platform):
        return self.m_platform_names.get(platform, None)

    def get_platform_names(self):
        return self.m_platform_names

    def get_build_x86_32(self):
        return self.m_build_x86_32

    def set_build_x86_32(self, build_x86_32):
        self.m_build_x86_32 = build_x86_32

    def get_build_x86_64(self):
        return self.m_build_x86_64

    def set_build_x86_64(self, build_x86_64):
        self.m_build_x86_64 = build_x86_64

    def set_call_esyssdkcli(self, call_esyssdkcli):
        self.m_call_esyssdkcli = call_esyssdkcli

    def get_call_esyssdkcli(self):
        return self.m_call_esyssdkcli

    def set_platform_toolset(self, platform_toolset):
        self.m_platform_toolset = platform_toolset

    def get_platform_toolset(self):
        if self.m_platform_toolset is not None:
            return self.m_platform_toolset

        if self.get_version() not in MSBuild.PLATFORM_TOOLSET:
            print("ERROR: version %s is not supported" % self.get_version())
            assert False
            return None

        self.m_platform_toolset = MSBuild.PLATFORM_TOOLSET[self.get_version()]
        return self.m_platform_toolset

    def set_targets(self, targets):
        self.m_targets = targets

    def get_targets(self):
        return self.m_targets

    def set_version(self, version):
        self.m_version = version

    def get_version(self):
        return self.m_version

    def set_sln_path(self, sln_path):
        self.m_sln_path = sln_path
        self.m_add_targets = True
        self.m_vcxproj_path = None

    def get_sln_path(self):
        return self.m_sln_path

    def get_add_targets(self):
        return self.m_add_targets

    def set_vcxproj_path(self, vcxproj_path):
        self.m_vcxproj_path = vcxproj_path
        self.m_add_targets = False
        self.m_sln_path = None

    def get_vcxproj_path(self):
        return self.m_vcxproj_path

    def set_display_env(self, set_display_env):
        self.m_set_display_env = set_display_env

    def get_display_env(self):
        return self.m_set_display_env

    def set_rel_output_folder(self, rel_output_folder):
        self.m_rel_output_folder = rel_output_folder

    def get_rel_output_folder(self):
        if self.m_rel_output_folder is not None:
            return self.m_rel_output_folder
        return self.get_dft_rel_output_folder()
    
    def get_dft_rel_output_folder(self):
        path = "bin" + os.sep
        plat = self.get_platform_toolset()
        if plat is None:
            return None
        path += "vc" + plat[1:]
        if self.get_build_x86_32():
            path += "_" + "x32"
        elif self.get_build_x86_64():
            path += "_" + "x64"
        else:
            return None
        path += "_" + "dll"
        return path
        
    def get_output_folder(self):
        rel_path = self.get_rel_output_folder()
        base_path = self.get_base_folder()
        if base_path is None:
            return None
        out_path = os.path.join(base_path, rel_path)
        return out_path

    def get_ms_build_cmd(self, build_type):
        version = self.get_version()
        if version not in MSBuild.MSVC_VAR_PATH:
            return None

        if build_type not in MSBuild.CMD_DETAILS:
            return None
        if build_type not in self.get_platform_names():
            return None

        cmd_details = MSBuild.CMD_DETAILS[build_type]
        platform_name = self.get_platform_name(build_type)

        bat_path = MSBuild.MSVC_VAR_PATH[version]
        # cmd = "setlocal && "
        cmd = ""
        cmd += '"' + bat_path + "\\" + cmd_details[0] + '" ' + cmd_details[1]
        cmd += " && project\\msvc\\set_env.bat"
        if self.m_display_env:
            cmd += " && set"
        if self.get_call_esyssdkcli():
            if "ESYSSDK_INST_DIR" not in os.environ:
                print("ERROR: env var ESYSSDK_INST_DIR is not defined.")
                return -1
            esyssdk_inst_dir = os.environ["ESYSSDK_INST_DIR"]
            #esyssdk_inst_dir = "C:\\project\\libesys\\esyssdk_dev\\bin\\vc141_x64_dll"
            #cmd += ' && "%s\\esyssdkcli" --call --' % esyssdk_inst_dir
            cmd += (" && \"%s\\bin\\esyssdkcli\" --call --" % esyssdk_inst_dir)
        else:
            cmd += " && "
        build_file = None
        if self.get_sln_path() is not None:
            build_file = self.get_sln_path()
        elif self.get_vcxproj_path() is not None:
            build_file = self.get_vcxproj_path()
        else:
            return None
        
        cmd += " MSBuild " + build_file + " -maxcpucount"
        cmd += (
            " /p:Configuration=ReleaseDll;PlatformToolset=%s"
            % self.get_platform_toolset()
        )
        cmd += ' /p:Platform="' + platform_name + '"'
        if self.get_build_no_dependencies():
            cmd += " /p:BuildProjectReferences=false"
        return cmd

    def build_x86_xx(self, build_type):
        cmd = self.get_ms_build_cmd(build_type)
        if cmd is None:
            return -1

        if self.get_add_targets():
            cmd_target = ""
            for target in self.get_targets():
                if cmd_target != "":
                    cmd_target += ";"
                if self.get_rebuild():
                    target += ":Rebuild"
                cmd_target += target
        else:
            if self.get_rebuild():
                cmd_target = "Rebuild"
            else:
                cmd_target = "Build"
        cmd += " /t:" + cmd_target

        print("cmd = %s" % cmd)
        result = subprocess.run(cmd, stdout=sys.stdout,
                                stderr=sys.stderr, env=self.get_env())
        if result.returncode != 0:
            print("ERROR %s while building" % result.returncode)
            return -result.returncode
        return 0

    def build_x86_64(self):
        return self.build_x86_xx(MSBuild.X86_64)

    def build_x86_32(self):
        return self.build_x86_xx(MSBuild.X86_32)

    def build(self):
        print("MSBbuild.build")
        print("cwd = %s" % os.getcwd())

        if self.get_build_x86_64():
            result = self.build_x86_64()
            if result < 0:
                return result

        if self.get_build_x86_32():
            result = self.build_x86_32()
            if result < 0:
                return result

        return 0

    def clean(self):
        print("MSBbuild.clean")
        print("cwd = %s" % os.getcwd())

        if self.get_build_x86_64():
            result = self.clean_x86_64()
            if result < 0:
                return result

        if self.get_build_x86_32():
            result = self.clean_x86_32()
            if result < 0:
                return result

        return 0

    def clean_x86_64(self):
        return self.clean_x86_xx(MSBuild.X86_64)

    def clean_x86_32(self):
        return self.clean_x86_xx(MSBuild.X86_32)

    def clean_x86_xx(self, build_type):
        cmd = self.get_ms_build_cmd(build_type)
        if cmd is None:
            return -1

        cmd += " /t:Clean"
        print("cmd = %s" % cmd)
        result = subprocess.run(cmd, stdout=sys.stdout, stderr=sys.stderr)
        if result.returncode != 0:
            print("ERROR %s while cleaning" % result.returncode)
            return -result.returncode
        return 0
