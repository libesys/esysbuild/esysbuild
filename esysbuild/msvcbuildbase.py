##
# __legal_b__
##
# Copyright (c) 2022-2023 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import argparse
import os
import sys
import subprocess
import colorama
from colorama import Fore, Back, Style

colorama.init()

from esysbuild.create_iss import CreateISS
import esyssdk

# This in only needed for SW developement of ESysCIBuild,
# when we want to be able to import ESysCIBuild from the
# super build or simply clone it
esyssdk.find_esysbuild(__file__)

import esysbuild  # noqa


class MsvcBuildBase:
    ACTION_BUILD = 0
    ACTION_CLEAN = 1
    ACTION_CONAN_INSTALL = 2
    ACTION_BUILD_CXX = 3
    ACTION_BUILD_PY = 4
    ACTION_BUILD_ARTIFACTS = 5
    ACTION_BUILD_ISS = 6
    ACTION_BUILD_INST = 7

    CODE_SIGNING_CERT_ENV = 0
    CODE_SIGNING_CERT_TEST = 1
    CODE_SIGNING_CERT_OPEN = 2
    CODE_SIGNING_CERT_ULTRADUINO = 3

    def __init__(self):
        self.m_build_x86_32 = True
        self.m_build_x86_64 = True
        self.m_action = MsvcBuildBase.ACTION_BUILD
        self.m_sln_path = None
        self.m_ms_build = esysbuild.MSBuild()
        self.m_ms_build.set_version("2017")
        self.m_ms_build.set_platform_name(esysbuild.MSBuild.X86_32, "x86")
        self.m_build_py = None
        self.m_base_rel_path = None
        self.m_sign = False
        self.m_build_python_modules = True
        self.m_python_vcxproj = None
        self.m_code_signing_cert = None
        self.m_parser = None
        self.m_iss_file = None
        self.m_base_path = None
        self.m_iss_template_file = None
        self.m_sign_env_var_name = None

    def set_sign_env_var_name(self, sign_env_var_name):
        self.m_sign_env_var_name = sign_env_var_name

    def get_sign_env_var_name(self):
        return self.m_sign_env_var_name
    
    def set_platform_name(self, platform, name):
        self.m_ms_build.set_platform_name(platform, name)
    
    def get_platform_name(self, platform):
        return self.m_ms_build.get_platform_name(platform)

    def set_base_path(self, base_path, rel_path=None):
        if os.path.isfile(base_path):
            base_path = os.path.dirname(base_path)
        if rel_path:
            base_path = os.path.join(base_path, rel_path)
        self.m_base_path = os.path.normpath(base_path)
        self.m_ms_build.set_base_folder(base_path)

    def get_base_path(self):
        return self.m_base_path

    def set_iss_template_file(self, iss_template_file):
        self.m_iss_template_file = iss_template_file

    def get_iss_template_file(self):
        return self.m_iss_template_file

    def set_iss_file(self, iss_file):
        self.m_iss_file = iss_file

    def get_iss_file(self):
        return self.m_iss_file

    def set_parser(self, parser):
        self.m_parser = parser
        self.m_ext_parser = True

    def get_parser(self):
        return self.m_parser

    def print_info(self, cmd):
        # Colorize a single line and then reset
        print(Fore.GREEN + cmd + Style.RESET_ALL)

    def print_cmd(self, cmd):
        # Colorize a single line and then reset
        print(Fore.BLUE + cmd + Style.RESET_ALL)

    def print_err(self, cmd):
        # Colorize a single line and then reset
        print(Fore.RED + cmd + Style.RESET_ALL)

    def set_build_python_modules(self, build_python_modules):
        self.m_build_python_modules = build_python_modules

    def get_build_python_modules(self):
        return self.m_build_python_modules

    def set_base_rel_path(self, base_rel_path):
        self.m_base_rel_path = base_rel_path
        # self.m_ms_build.set_base_folder(self.get_abs_path("."))

    def get_base_rel_path(self):
        return self.m_base_rel_path

    def get_abs_path(self, rel_path):
        if self.get_base_path() is not None:
            path = os.path.join(self.get_base_path(), rel_path)
        else:
            path = rel_path
        abs_path = os.path.abspath(path)
        return abs_path

    def set_sln_path(self, set_sln_path):
        self.m_sln_path = set_sln_path

    def get_sln_path(self):
        return self.m_sln_path

    def set_targets(self, targets):
        self.m_targets = targets
        self.m_ms_build.set_targets(targets)

    def get_targets(self):
        return self.m_targets

    def set_python_vcxproj(self, python_vcxproj):
        self.m_python_vcxproj = python_vcxproj

    def get_python_vcxproj(self):
        return self.m_python_vcxproj

    def set_sign(self, sign):
        self.m_sign = sign

    def get_sign(self):
        return self.m_sign

    def set_code_signing_cert(self, code_signing_cert):
        self.m_code_signing_cert = code_signing_cert

    def get_code_signing_cert(self):
        return self.m_code_signing_cert

    def get_build_x86_32(self):
        return self.m_build_x86_32

    def set_build_x86_32(self, build_x86_32):
        self.m_build_x86_32 = build_x86_32

    def get_build_x86_64(self):
        return self.m_build_x86_64

    def set_build_x86_64(self, build_x86_64):
        self.m_build_x86_64 = build_x86_64

    def set_action(self, action):
        self.m_action = action

    def get_action(self):
        return self.m_action

    def get_ms_build(self):
        return self.m_ms_build

    def add_param(self) -> None:
        """Process the command line parameters"""
        if self.get_parser() is None:
            self.set_parser(argparse.ArgumentParser())
            parser = self.get_parser()
        else:
            parser = self.get_parser().add_argument_group("Build")
        parser.add_argument(
            "--clean", dest="m_clean", action="store_true", help="clean the build(s)"
        )
        parser.add_argument(
            "--msvc",
            dest="m_msvc",
            action="store",
            default=None,
            help="Visual c++ version: 2017, 2022",
        )
        parser.add_argument(
            "--build_inst",
            dest="build_inst",
            action="store_true",
            help="build the installer",
        )
        parser.add_argument(
            "--build_py",
            dest="m_build_py",
            nargs="?",
            action="store",
            const="",
            default=None,
            help="build only the python biding(s)",
        )
        parser.add_argument(
            "--sign", dest="sign", action="store_true", help="sign the installer"
        )
        parser.add_argument(
            "--build_iss",
            dest="build_iss",
            action="store_true",
            help="build the ISS config script",
        )
        parser.add_argument(
            "--sign_cert",
            dest="sign_cert",
            action="store",
            nargs="?",
            const="test",
            default="test",
            help="sign_cert: test, open, ultraduino",
        )
        # parser.add_argument("--help", dest="m_help",
        #                    action="store_true", help="print help")

    def parse_param(self):
        args = self.get_parser().parse_args()
        self.m_args = args

    def process_param(self, args=None):
        if args is None:
            args = self.m_args
        if args.m_clean:
            self.set_action(MsvcBuildBase.ACTION_CLEAN)
        if args.m_build_py is not None:
            self.print_info("build_py = '%s'" % args.m_build_py)
            self.set_action(MsvcBuildBase.ACTION_BUILD_PY)
            self.m_build_py = args.m_build_py
        if args.build_inst:
            self.set_action(MsvcBuildBase.ACTION_BUILD_INST)
        if args.build_iss:
            self.set_action(MsvcBuildBase.ACTION_BUILD_ISS)
        if args.m_msvc:
            self.m_ms_build.set_version(args.m_msvc)
        if args.sign:
            self.set_sign(True)
        if args.sign_cert is None:
            self.set_code_signing_cert(MsvcBuildBase.CODE_SIGNING_CERT_ENV)
        elif args.sign_cert == "test":
            self.set_code_signing_cert(MsvcBuildBase.CODE_SIGNING_CERT_TEST)
        elif args.sign_cert == "open":
            self.set_code_signing_cert(MsvcBuildBase.CODE_SIGNING_CERT_OPEN)
        elif args.sign_cert == "ultraduino":
            self.set_code_signing_cert(MsvcBuildBase.CODE_SIGNING_CERT_ULTRADUINO)

    def handle_cmd_line(self):
        self.add_param()
        self.parse_param()
        self.process_param()

    def do(self):
        self.print_info("Build")
        self.print_info("cwd = %s" % os.getcwd())

        # self.handle_cmd_line()

        self.get_ms_build().set_build_x86_32(self.get_build_x86_32())
        self.get_ms_build().set_build_x86_64(self.get_build_x86_64())

        if self.get_action() == MsvcBuildBase.ACTION_CLEAN:
            return self.clean()
        elif self.get_action() == MsvcBuildBase.ACTION_BUILD:
            return self.build()
        elif self.get_action() == MsvcBuildBase.ACTION_BUILD_PY:
            return self.build_py()
        elif self.get_action() == MsvcBuildBase.ACTION_BUILD_ARTIFACTS:
            return self.buid_artifacts()
        elif self.get_action() == MsvcBuildBase.ACTION_BUILD_ISS:
            return self.build_iss()
        elif self.get_action() == MsvcBuildBase.ACTION_BUILD_INST:
            return self.build_inst()

    def clean(self):
        return self.get_ms_build().clean()

    def build(self):
        self.print_info("build")
        result = self.conan_install()
        if result < 0:
            return result

        result = self.build_cxx()
        if result < 0:
            return result

        result = self.build_py()
        if result < 0:
            return result

        result = self.build_iss()
        if result < 0:
            return result

        return self.buid_artifacts()

    def conan_install(self):
        self.print_info("conan_install: begin ...")
        cmd = "project\\msvc141\\conan_install.bat"
        self.print_cmd("cmd = %s" % cmd)
        result = subprocess.run(cmd, stdout=sys.stdout, stderr=sys.stderr, shell=True)
        if result.returncode != 0:
            self.print_err(
                "ERROR %s while installing conan libraries" % result.returncode
            )
            self.print_info("conan_install: end.")
            return -result.returncode
        self.print_info("conan_install: end.")
        return 0

    def build_cxx(self):
        self.print_info("build_cxx: begin ...")
        abs_sln_path = os.path.join(self.get_base_path(), self.get_sln_path())
        self.get_ms_build().set_sln_path(abs_sln_path)
        result = self.get_ms_build().build()
        self.print_info("build_cxx: end.")
        return result

    def build_py(self):
        self.print_info("build_py: begin ...")
        if self.get_python_vcxproj() is None:
            self.print_info("Nothing to do.")
            self.print_info("build_py: end.")
            return 0
        list_py = esysbuild.ListPy()
        pys = list_py.get_list()

        build_py = self.m_build_py
        if build_py is None or build_py == "":
            py_to_builds = pys
        else:
            build_py = build_py.replace(":", ";")
            py_ver_to_build_inputs = self.m_build_py.split(";")
            py_ver_to_builds = []
            for py_ver_to_build in py_ver_to_build_inputs:
                version = py_ver_to_build
                plat_idx = version.find("-")
                if plat_idx == -1:
                    py = list_py.find(version, esysbuild.Py.PLAT_X64)
                    if py is not None:
                        py_to_builds.append(py)
                    else:
                        self.print_err(
                            "ERROR: Python version %s 64 bits is not installed"
                            % version
                        )
                    py = list_py.find(version, esysbuild.Py.PLAT_X32)
                    if py is not None:
                        py_to_builds.append(py)
                    else:
                        self.print_err(
                            "ERROR: Python version %s 32 bits is not installed"
                            % version
                        )
                else:
                    plat_txt = version[plat_idx + 1 :]
                    version = version[:plat_idx]
                    if plat_txt == "32":
                        py = list_py.find(version, esysbuild.Py.PLAT_X32)
                        if py is not None:
                            py_to_builds.append(py)
                        else:
                            self.print_err(
                                "ERROR: Python version %s 64 bits is not installed"
                                % version
                            )
                    elif plat_txt == "64":
                        py = list_py.find(version, esysbuild.Py.PLAT_X64)
                        if py is not None:
                            py_to_builds.append(py)
                        else:
                            self.print_err(
                                "ERROR: Python version %s 64 bits is not installed"
                                % version
                            )

        if self.get_build_python_modules():
            self.print_info("For the Python versions:")
            for py_to_build in py_to_builds:
                self.print_info("    %s" % py_to_build.get_version())
            for py_to_build in py_to_builds:
                for py_vcxproj in self.m_python_vcxproj:
                    result = self.build_py_version(py_to_build, py_vcxproj)
                    if result < 0:
                        self.print_err(
                            "ERROR: couldn't build binding for Python %s"
                            % py_to_build.get_version()
                        )
        result = self.write_python_shadows(py_to_builds)
        self.print_info("build_py: end.")
        return result

    def write_python_shadows(self, py_to_builds):
        plat_dict = {}
        plat_list = []
        for py_to_build in py_to_builds:
            plat = py_to_build.get_plat()
            if plat not in plat_dict:
                plat_list.append(plat)
                plat_dict[plat] = 1

        for plat in plat_list:
            if plat == esysbuild.Py.PLAT_X64:
                self.get_ms_build().set_build_x86_32(False)
                self.get_ms_build().set_build_x86_64(True)
            elif plat == esysbuild.Py.PLAT_X32:
                self.get_ms_build().set_build_x86_32(True)
                self.get_ms_build().set_build_x86_64(False)
            else:
                return -1
            for py_vcxproj in self.m_python_vcxproj:
                result = self.write_python_shadow(py_vcxproj, py_to_builds, plat)
                if result < 0:
                    return result
        return 0

    def write_python_shadow(self, py_vcxproj, py_to_builds, plat):
        output_path = self.get_ms_build().get_output_folder()
        if output_path is None:
            return -1
        file_path = os.path.join(output_path, py_vcxproj[0] + ".py")
        fout = open(file_path, "wt")
        fout.write("import sys\n")
        fout.write("\n")
        fout.write(
            """py_ver = "%s.%s" % (sys.version_info.major, sys.version_info.minor)\n"""
        )
        count = 0
        for py_to_build in py_to_builds:
            if py_to_build.get_plat() != plat:
                continue
            py_ver = py_to_build.get_version()
            if count == 0:
                str = 'if py_ver == "'
            else:
                str = 'elif py_ver == "'
            count += 1
            str += py_ver + '":\n'
            fout.write(str)
            py_ver_ = py_ver.replace(".", "_")
            str = "    from " + py_vcxproj[0] + "_py" + py_ver_ + " import *\n"
            fout.write(str)
        # if py_ver == "3.10":
        #    from esyslog_py3_10 import *
        # elif py_ver == "3.9":
        #    from esyslog_py3_9 import *
        # elif py_ver == "3.8":
        #    from esyslog_py3_8 import *
        # elif py_ver == "3.7":
        #    from esyslog_py3_7 import *
        # elif py_ver == "3.6":
        #    from esyslog_py3_6 import *
        fout.close()
        return 0

    def build_py_version(self, py, py_vcxproj):
        if py.get_plat() == esysbuild.Py.PLAT_X64:
            plat_ver = "64 bits"
        else:
            plat_ver = "32 bits"
        self.print_info(
            "build_py_version %s py%s %s: begin ..." % (py_vcxproj[0], py.get_version(), plat_ver)
        )
        py_vcxproj_path = py_vcxproj[1]
        self.m_ms_build.set_vcxproj_path(py_vcxproj_path)
        self.get_ms_build().set_build_no_dependencies(True)
        self.get_ms_build().set_rebuild(True)
        self.get_ms_build().set_env(os.environ)
        version_ = py.get_version()
        version_ = version_.replace(".", "_")
        self.get_ms_build().add_env_var("SWIG_TARGET_PYTHON_VERSION", version_)
        self.get_ms_build().add_env_var("PYESYS_UNIV_DLL_SUFFIX", "_py" + version_)
        self.get_ms_build().add_env_var(
            "PYESYS_UNIV_WARP_PREFIX", "py" + version_ + "_"
        )
        inc = os.path.join(py.get_folder_path(), "include")
        self.get_ms_build().add_env_var("ESYS_PYTHON_INC", inc)
        lib = os.path.join(py.get_folder_path(), "libs")
        self.get_ms_build().add_env_var("ESYS_PYTHON_LIB", lib)
        if py.get_plat() == esysbuild.Py.PLAT_X64:
            self.get_ms_build().set_build_x86_32(False)
            self.get_ms_build().set_build_x86_64(True)
        else:
            self.get_ms_build().set_build_x86_32(True)
            self.get_ms_build().set_build_x86_64(False)

        result = self.get_ms_build().build()
        if result < 0:
            self.print_error(
                "build_py_version %s py%s %s: error %s." % (py_vcxproj[0], py.get_version(), plat_ver, result)
            )
            return result
        self.print_info("build_py_version %s py%s %s: end." % (py_vcxproj[0], py.get_version(), plat_ver))
        return 0

    def get_signtool_config(self):
        if not self.get_sign():
            return [0, ""]
        if self.get_code_signing_cert() == MsvcBuildBase.CODE_SIGNING_CERT_ENV:
            signtool = os.environ.get("SIGNTOOL_LIBESYS", "")
            return [0, signtool]
        elif self.get_code_signing_cert() == MsvcBuildBase.CODE_SIGNING_CERT_TEST:
            signtool = "signtool sign /n $qTest UltraDuino$q /fd sha256 /tr $qhttp://timestamp.comodoca.com?td=sha256$q /td sha256 /s MY /as $f"
            return [0, signtool]
        elif self.get_code_signing_cert() == MsvcBuildBase.CODE_SIGNING_CERT_OPEN:
            signtool = "signtool sign /n $qOpen Source Developer$q /t $qhttp://time.certum.pl/$q /fd sha256 $f"
            return [0, signtool]
        elif self.get_code_signing_cert() == MsvcBuildBase.CODE_SIGNING_CERT_ULTRADUINO:
            return [-1, ""]
        else:
            return [-1, ""]

    def build_iss(self):
        self.print_info("build_iss: begin ...")
        base_path = self.get_base_path()
        if base_path is None:
            return -1
        iss = CreateISS()
        iss.set_base_path(base_path)
        if self.get_iss_template_file() is not None:
            iss.set_template_file(self.get_iss_template_file())
        iss.set_sign(self.get_sign())
        iss_file = self.get_iss_file()
        output_folder = os.path.dirname(iss_file)
        iss.set_output_folder(output_folder)
        result = iss.do()
        if result != 0:
            self.print_err("ERROR %s while building" % result)
            return result
        self.print_info("build_iss: end.")
        return 0

    def build_inst(self):
        result = self.build_iss()
        if result < 0:
            return result

        return self.buid_artifacts()

    def buid_artifacts(self):
        self.print_info("build_artifacts: begin ...")
        if self.get_iss_file() is None:
            self.print_err("ERROR no iss file provided")
            return -1
        result, signtool = self.get_signtool_config()
        cmd = "project\\msvc\\set_env.bat"
        if self.get_sign():
            cmd += " && set %s=1" % self.get_sign_env_var_name()
        # else:
        #     cmd += " && set ESYSREPO_SIGN=0"
        cmd += ' && "C:\\Program Files (x86)\\Inno Setup 6\\iscc" /Qp'

        if self.get_sign():
            cmd += ' "/Sdefault=%s"' % signtool
        iss_file = self.get_iss_file()
        iss_file_abs = self.get_abs_path(iss_file)
        cmd += " %s" % iss_file_abs

        self.print_cmd("cmd = %s" % cmd)
        result = subprocess.run(cmd, stdout=sys.stdout, stderr=sys.stderr, shell=True)
        if result.returncode != 0:
            self.print_err("ERROR %s while building" % result.returncode)
            return -result.returncode
        self.print_info("build_artifacts: end.")
        return 0
