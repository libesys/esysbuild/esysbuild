## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

class GroupMngrBase(object):
    def __init__(self,**kwds):
        super().__init__(**kwds)
        self.m_group_names_list=[]
        self.m_group_names_dict={}
    def AddGroupName(self, group_name):
        typ_=type(group_name)
        if typ_==str:
            self.m_group_names_dict[group_name]=None
            self.m_group_names_list.append(group_name)
        elif typ_==list:
            for name in group_name:
                if name in self.m_group_names_dict:
                    continue
                self.m_group_names_dict[name]=None
                self.m_group_names_list.append(name)
        else:
            pass
    def GetGroupDict(self):
        return self.m_group_names_dict
    def GetGroupList(self):
        return self.m_group_names_list                    
    def GetGroupCount(self):
        return len(self.m_group_names_list)
    def GetGroupName(self, idx):
        if idx>=self.GetGroupCount():
            return None
        return self.m_group_names_list[idx]
    def GetGroup(self, idx):
        if idx>=self.GetGroupCount():
            return None
        return self.m_group_names_list[idx]
    def Intersection(self, group_mngr):
        if type(group_mngr)==GroupMngrBase:
            the_list=group_mngr.GetGroupList()
        elif type(group_mngr)==list:
            the_list=group_mngr
        result=set(self.m_group_names_list).intersection(the_list)
        return list(result)

