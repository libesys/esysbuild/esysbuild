## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

from esysbuild.dynmod import *
 
class BuildFile(DynMod):
    def __init__(self, **kwds):
        super().__init__(**kwds)
        self.m_boards_rel_dir=None
        self.m_builds_rel_dir=None
        self.m_bootstrap_script=None
        self.m_env_var_rel_dir_dict={}
    def AddEnvVarRelDir(self, env_var_rel_dir, value):
        self.m_env_var_rel_dir_dict[env_var_rel_dir]=value
    def GetEnvVarRelDirList(self):
        return list(self.m_env_var_rel_dir_dict.items())
    def AddGerrits(self, gerrit_mgr):
        return 0
    def AddLibs(self, lib_mngr):
        return 0    
    def SetBoardsRelDir(self, boards_rel_dir):
        self.m_boards_rel_dir=os.path.normpath(boards_rel_dir)
    def GetBoardsRelDir(self):
        return self.m_boards_rel_dir
    def SetBuildsRelDir(self, builds_rel_dir):
        self.m_builds_rel_dir=os.path.normpath(builds_rel_dir)
    def GetBuildsRelDir(self):
        return self.m_builds_rel_dir
    def GetBuildsDir(self):
        path=os.path.split(self.GetPath())[0]
        return os.path.join(path,self.GetBuildsRelDir())
    def GetBoardsDir(self):
        path=os.path.split(self.GetPath())[0]
        return os.path.join(path,self.GetBoardsRelDir())
    def SetBootStrapScript(self, bootstrap_script):
        self.m_bootstrap_script=bootstrap_script
    def GetBootStrapScript(self):
        return self.m_bootstrap_script
    
    
    @staticmethod        
    def Load(path, name="esysbuild.cfg"):
        return DynMod.Load(path, "esysbuild.cfg", "GetBuildFile")        

