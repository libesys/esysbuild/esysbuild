## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os

from esysbuild.libmngr import *
from esysbuild.lib import *
from esysbuild.buildfile import *
from esysbuild.groupmngrbase import *

class Repo(LibMngr, GroupMngrBase):
    LINK_INSTALLED = Lib.LINK_INSTALLED
    LINK_LIBTOOL = Lib.LINK_LIBTOOL
    LINK_PKG = Lib.LINK_PKG
    LINK_DFLT = Lib.LINK_DFLT
    def __init__(self, name, ext=False, location=None, **kwds):
        super().__init__(**kwds)
        self.m_base_dir=None
        self.m_lib_mngr=None
        LibMngr.SetName(self, name)
        self.m_is_external=ext
        self.m_location=location
        self.m_build_file=None
        self.m_link_cfg=None
        self.m_prefix=None
        self.m_include_path_dict={}
        self.m_dependencies_list=[]
        self.m_dependencies_dict={}        
        if location==None:
            if ext==False:
                self.m_location="src/%s" % name
            else:
                self.m_location="extlib/%s" % name
        else:
            self.m_location=location   
    def SetLinkCfg(self, link_cfg):
        self.m_link_cfg=link_cfg
    def GetLinkCfg(self):        
        return self.m_link_cfg                    
    def SetLibMngr(self, lib_mngr):
        self.m_lib_mngr=lib_mngr
    def GetLibMngr(self):
        return self.m_lib_mngr
    def SetBaseDir(self, base_dir):
        self.m_base_dir=base_dir
    def GetBaseDir(self):
        return self.m_base_dir    
    def SetLocation(self, location):
        self.m_location=location
    def GetLocation(self):
        return self.m_location
    def GetDir(self):
        return os.path.join(self.GetBaseDir(),self.GetLocation())
    def SetIsExternal(self, is_external):
        self.m_is_external=is_external
    def IsExternal(self):
        return self.m_is_external
    def SetBuildFile(self, build_file):
        self.m_build_file=build_file
    def GetBuildFile(self):
        return self.m_build_file
    def SetPrefix(self, prefix):
        self.m_prefix=prefix
    def GetPrefix(self):
        return self.m_prefix
    def Add(self, lib):
        LibMngr.Add(self, lib)
        lib.SetRepo(self)
        lib_mngr=self.GetLibMngr()
        if lib_mngr!=None:
            lib_mngr.Add(lib)
        self.AddGroupName(lib.GetGroupList())
    def LoadLibs(self):
        path=os.path.join(self.GetBaseDir(), os.path.normpath(self.GetLocation()), "esysbuild.cfg.py")
        path=os.path.normpath(path)
        if os.path.exists(path)==False:
            self.Verbose(0,"WARNING: the following path doesn't exist %s" %path)
            return -1
        result, build_file=BuildFile.Load(path)
        if result==0 and build_file!=None:
            self.SetBuildFile(build_file)
            build_file.AddLibs(self)
        return result
    def AddIncPath(self, include_path, cfg_link=Lib.LINK_DFLT):
        if (cfg_link in self.m_include_path_dict)==False:
            self.m_include_path_dict[cfg_link]=[]
        self.m_include_path_dict[cfg_link].append(include_path)
    def GetIncPathCount(self, cfg_link=Lib.LINK_DFLT):
        if (cfg_link in self.m_include_path_dict)==False:
            return 0
        return len(self.m_include_path_dict[cfg_link]==False)
    def GetIncPaths(self, cfg_link=Lib.LINK_DFLT):
        if (cfg_link in self.m_include_path_dict)==False:
            return None
        return self.m_include_path_dict[cfg_link]
    def GetIncPath(self, idx, cfg_link=Lib.LINK_DFLT):
        if (cfg_link in self.m_include_path_dict)==False:
            return None        
        if idx>=self.GetIncPathCount(cfg_link):
            return None
        return self.m_include_path_dict[cfg_link][idx]
    def AddDependency(self, repo):
        if (repo.GetName() in self.m_dependencies_dict)==True:
            return
        self.m_dependencies_dict[repo.GetName()]=repo
        self.m_dependencies_list.append(repo)
    def AddDependencies(self, repos):
        if type(repos)==Repo:
            self.AddDependency(repos)
        elif type(repos)==list:
            for repo in repos:
                self.AddDependency(repo)        
    def GetDependenciesList(self):
        return self.m_dependencies_list
    def GetDependenciesDict(self):
        return self.m_dependencies_dict
    
        
                          
