## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import subprocess
import json
from esysbuild.change import *
from builtins import str

class Gerrit:
    def __init__(self, name=None, url=None, port=29418):
        self.m_name=name
        self.m_url=url
        self.m_port=port
    def SetName(self, name):
        self.m_name=name
    def GetName(self):
        return self.m_name
    def SetURL(self, url):
        self.m_url=url
    def GetURL(self):
        return self.m_url
    def SetPort(self, port):
        self.m_port=port
    def GetPort(self):
        return self.m_port
    
    def GetChanges(self, topic, only_open=True):
        if type(topic)==str:
            patch_sets=topic
        elif type(topic)==list:
            patch_sets=""            
            for item in topic:
                patch_sets+=" "+item
                
        cmd=["ssh", "-p %i" % self.GetPort(), self.GetURL(), "gerrit",  "query", "--patch-sets %s" % patch_sets, "--format JSON"]
        if only_open==True:
            cmd+=["status:open"]
        result = subprocess.check_output(cmd)
        if type(result)==bytes:
            result=result.decode('utf-8')
        change_list=[]
        changes=result.split("\n")        
        decoder=json.JSONDecoder()
        if len(changes)==0:
            return None
        result=changes[-1]    #Last should the result
        last=-1        
        if result.strip()=="":
            result=changes[-2]
            last=-2
        try:
            pyobj=decoder.decode(result)
        except:
            pyobj=None
            print("ERROR: could decode JSON answer from gerrit")
            return -3
        if ("rowCount" in pyobj)==False:
            print("ERROR: could decode JSON answer from gerrit")
            return -4
        if pyobj["rowCount"]==0: 
            return []
        changes=changes[:last]        
        for change in changes:
            if change.strip()=="":
                continue
            try:
                pyobj=decoder.decode(change)
            except:
                pyobj=None
                print("ERROR: could decode JSON from gerrit")
            if pyobj!=None:
                change_obj=Change()
                change_obj.SetGerrit(self)
                change_obj.SetJSON(change)
                change_obj.SetPy(pyobj)
                change_list.append(change_obj)
        return change_list
            
        
        