## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

class RepoMngr:
    def __init__(self):
        self.m_base_dir=None
        self.m_lib_mngr=None
        self.m_repo_list=[]
        self.m_repo_dict={}
        self.m_repo_loc_dict={}
        self.m_verbose_obj=None
        self.m_dft_link_cfg=None
    def SetDftLinkCfg(self, dft_link_cfg):
        self.m_dft_link_cfg=dft_link_cfg
    def GetDftLinkCfg(self):
        return self.m_dft_link_cfg
    def SetVerboseObj(self, verbose_obj):
        self.m_verbose_obj=verbose_obj
    def GetVerboseObj(self):
        return self.m_verbose_obj
    def Verbose(self, verbose, txt, start=False, end=False):
        if self.m_verbose_obj==None:
            return
        self.m_verbose_obj.Verbose(verbose, txt, start, end)        
    def SetLibMngr(self, lib_mngr):
        self.m_lib_mngr=lib_mngr
    def GetLibMngr(self):
        return self.m_lib_mngr
    def SetBaseDir(self, base_dir):
        self.m_base_dir=base_dir
    def GetBaseDir(self):
        return self.m_base_dir
    def Add(self, repo):
        if repo.GetLinkCfg()==None:
            repo.SetLinkCfg(self.GetDftLinkCfg())
        self.m_repo_list.append(repo)
        self.m_repo_dict[repo.GetName()]=repo
        self.m_repo_loc_dict[repo.GetLocation()]=repo
        if self.GetLibMngr()!=None:
            self.GetLibMngr().AddMngr(repo)
    def GetCount(self):
        return len(self.m_repo_list)
    def Get(self, idx):
        if idx>=self.GetCount():
            return None
        return self.m_repo_list[idx]
    def LoadAllLibs(self):
        for repo in self.m_repo_list:
            repo.SetBaseDir(self.GetBaseDir())
            repo.SetLibMngr(self.GetLibMngr())
            repo.SetVerboseObj(self.GetVerboseObj())
            repo.LoadLibs()        
    def Find(self, name_or_loc):
        if name_or_loc in self.m_repo_dict:
            return self.m_repo_dict[name_or_loc]
        if name_or_loc in self.m_repo_loc_dict:
            return self.m_repo_loc_dict[name_or_loc]
        return None
    


