## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

from esysbuild.lib import *

class Exe(Lib):
    def __init__(self, name, ext=False, location=None):
        super().__init__(name, ext, location)