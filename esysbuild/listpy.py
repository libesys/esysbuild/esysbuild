##
# __legal_b__
##
# Copyright (c) 2022 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
##
# __legal_e__
##

import os
import subprocess


class Py:
    PLAT_X64 = 0
    PLAT_X32 = 1

    def __init__(self):
        self.m_version = None
        self.m_bin_path = None
        self.m_folder_path = None
        self.m_plat = None
        self.m_default = False

    def set_version(self, version):
        self.m_version = version

    def get_version(self):
        return self.m_version

    def set_bin_path(self, bin_path):
        self.m_bin_path = bin_path

    def get_bin_path(self):
        return self.m_bin_path

    def get_folder_path(self):
        if self.m_bin_path is None:
            return None
        return os.path.dirname(self.m_bin_path)

    def set_plat(self, plat):
        self.m_plat = plat

    def get_plat(self):
        return self.m_plat

    def set_default(self, default):
        self.m_default = default

    def get_default(self):
        return self.m_default


class ListPy:
    def __init__(self):
        self.m_pys_dict = None
        self.m_pys_list = None

    def get_list(self):
        if self.m_pys_list is not None:
            return self.m_pys_list

        self.m_pys_dict = {}
        self.m_pys_list = []
        cmd = "py --list-paths"
        result = subprocess.run(cmd, capture_output=True)
        if result.returncode != 0:
            return None
        text = result.stdout.decode("ascii")
        lines = text.splitlines(keepends=False)

        return self.parse_lines(lines)

    def get_vers(self):
        pys = self.get_list()
        l = []
        for py in pys:
            ver = py.get_version()
            if py.get_plat() == Py.PLAT_X32:
                ver += "-32"
            l.append(ver)
        return l

    def parse_lines(self, lines):
        for line in lines:
            i = line.find("-V")
            if i == -1:
                continue
            i = line.find(":")
            if i != -1:
                line = line[i+1:]
            tokens = line.split()
            if len(tokens) == 0:
                continue
            version = tokens[0]
            if version[0] == '-':
                version = version[1:]
            idx = version.find("-")
            if idx == -1:
                py_ver = version
                py_plat = "64"
            else:
                py_ver = version[:idx]
                py_plat = version[idx+1:]
            py = Py()
            py.set_version(py_ver)
            if py_plat == "64":
                py.set_plat(Py.PLAT_X64)
            else:
                py.set_plat(Py.PLAT_X32)
            path_with_spaces = None
            default = False
            path_idx = 1
            if len(tokens) == 2:
                path_with_spaces = False
            elif (len(tokens) == 3) and (tokens[1] == "*"):
                path_with_spaces = False
                default = True
                path_idx = 2
            else:
                path_with_spaces = True
            if not path_with_spaces:
                py.set_bin_path(tokens[path_idx])
            else:
                if tokens[-1] == "*":
                    default = True
                    tokens = tokens[:-1]
                tokens = tokens[1:]
                path = ""
                for token in tokens:
                    if path != "":
                        path += " "
                    path += token
                py.set_bin_path(path)
            py.set_default(default)
            self.m_pys_list.append(py)
            if version not in self.m_pys_dict:
                self.m_pys_dict[version] = {}
            self.m_pys_dict[version][py.get_plat()] = py
        return self.m_pys_list

    def print_list(self):
        if (self.m_pys_list is None) or (len(self.m_pys_list) == 0):
            print("No Python versions found.")
        print("Found following Python versions:")
        for py_ver in self.m_pys_list:
            print("%s", py_ver.get_version())


    def find(self, version, plat=Py.PLAT_X64):
        if self.m_pys_dict is None:
            return None
        if version not in self.m_pys_dict:
            return None
        if plat not in self.m_pys_dict[version]:
            return None
        return self.m_pys_dict[version][plat]
