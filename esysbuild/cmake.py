import os
import sys
import subprocess
import platform
from esysbuild import utils

class CMake:
    def __init__(self, esysbuild=None):
        self.m_esysbuild=esysbuild
        self.m_build_dir=None
        self.m_root_dir=None
    def SetESysBuild(self, esysbuild):
        self.m_esysbuild=esysbuild
    def GetESysBuild(self):
        return self.m_esysbuild
    def SetBuildDir(self, build_dir):
        self.m_build_dir=build_dir
    def GetBuildDir(self):
        return self.m_build_dir
    def SetRootDir(self, root_dir):
        self.m_root_dir=root_dir
    def GetRootDir(self):
        return self.m_root_dir
    def Configure(self):
        if self.m_build_dir!=None:
            if os.path.exists(self.m_build_dir)==False:
                os.makedirs(self.m_build_dir)
            os.chdir(self.m_build_dir)
        cmake_path=os.path.relpath(self.GetRootDir(), self.m_build_dir)
        
        #DEVNULL = open(os.devnull, 'wb')
        cmd="cmake "+cmake_path
        try:
            #configure=subprocess.check_output(cmd, stderr=DEVNULL, shell=True)
            #configure=subprocess.check_output(cmd, shell=True)
            configure=subprocess.check_call(cmd, stderr = subprocess.STDOUT, shell=True)
        except subprocess.CalledProcessError:
            configure=None
        #DEVNULL.close()
    def WriteConfigShellScript(self, config_list):
        #Assumes moved to target folder
        fout=open("configure.sh", "wt")        
        for config_item_idx in range(len(config_list)):
            config_item=config_list[config_item_idx]
            config_item=config_item.replace("\\","/")
            if config_item_idx!=0:
                fout.write(" ")
            fout.write(config_item)
        fout.write("\n")
        fout.close()
        utils.MakeExecutable("configure.sh")
    def WriteConfigBatScript(self, config_list):
        #Assumes moved to target folder
        fout=open("configure.bat", "wt")        
        for config_item_idx in range(len(config_list)):
            config_item=config_list[config_item_idx]            
            if config_item_idx!=0:
                fout.write(" ")
            fout.write(config_item)
        fout.write("\n")
        fout.close()
    
    def CreateEclipse(self, folder, build, compiler):
        if folder!=None:
            if os.path.exists(folder)==False:
                os.makedirs(folder)
            print("Move to folder %s" % folder)
            os.chdir(folder)
            
        #print("cwd = %s" % os.getcwd())            
        cmake_path=os.path.relpath(self.GetRootDir(), folder)        
        toolchain_file=os.path.join(self.GetRootDir(), "project", "cmake", build.GetFullName()+"-"+compiler.GetFamilyName(), "toolchain.cmake")
        toolchain_file=os.path.relpath(toolchain_file, folder)
        dev_build=build.GetFullName()+"-"+compiler.GetFamilyName()
        if compiler.GetToolchainPath()!=None:        
            toolchain_path=compiler.GetToolchainPath().replace("\\","/")
        else:
            toolchain_path=None
        
        sys_plat=sys.platform
        plat_sys=platform.system()
        use_native_cmake=False;
        if plat_sys.find("MINGW")!=-1:
            use_native_cmake=True
        
        if ("ESYSBUILD_CMAKE" in os.environ) and (use_native_cmake==False): 
            cmake_dir=os.environ["ESYSBUILD_CMAKE"]
            cmd_list=[os.path.join(cmake_dir, "bin", "cmake")]
            cmake_gen="\"Eclipse CDT4 - Unix Makefiles\""
        else:
            cmd_list=["cmake"]
            cmake_gen="\"Eclipse CDT4 - Unix Makefiles\""
        #cmd_list.append("--trace-expand")
        #cmd_list.append("--debug-output")
        cmd_list.append("-G")
        cmd_list.append(cmake_gen)
        if self.GetESysBuild().GetDebugBuild()==True:
            cmd_list.append("-DCMAKE_BUILD_TYPE=Debug")
        else:
            cmd_list.append("-DCMAKE_BUILD_TYPE=Release")
        
        if os.path.exists(toolchain_file):
            cmd_list.append("-DCMAKE_TOOLCHAIN_FILE=" + toolchain_file)
        cmd_list.append("-DESYS_DEV_BUILD=" + dev_build)
        if (sys_plat=="msys") and (toolchain_path!=None):
            toolchain_path=toolchain_path.replace("\\","/")
            toolchain_path=toolchain_path.replace(":","/")
            toolchain_path="/"+toolchain_path
        if toolchain_path!=None:
            cmd_list.append("-DTARGET_TOOLCHAIN_PATH=" + toolchain_path)        
        if sys_plat=="win32" or sys_plat=="msys":
            cmd_list.append("-DTARGET_TOOLCHAIN_WIN_EXT=1")
        if sys_plat=="win32":
            if "ESYSBUILDSDK" in os.environ:
                make_dir=os.environ["ESYSBUILDSDK"]
                make_exe=os.path.join(make_dir, "extbin", "make.exe")
            else:
                make_exe="C:/prog/msys64/usr/bin/make.exe"
            cmd_list.append("-DCMAKE_MAKE_PROGRAM=" + make_exe)            
        #cmake_path=cmake_path.replace("/","\\")
        cmd_list.append(cmake_path)
        #cmd_list.append(" >cmake_trace_py.txt 2>&1")
        #print(cmd_list)
        self.WriteConfigShellScript(cmd_list)
        self.WriteConfigBatScript(cmd_list)
        if utils.IsShellScript():
            cmd_list="./configure.sh"
        else:
            cmd_list="configure.bat"
        try:
            configure=subprocess.check_call(cmd_list, stderr = subprocess.STDOUT, cwd=folder, shell=True)
        except subprocess.CalledProcessError:
            configure=None
    