## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import glob
from esysbuild.build import *

class BuildMngr:
    def __init__(self):
        self.m_search_paths=[]
        self.m_build_list=[]
        self.m_build_dict={}
        self.m_verbose_obj=None
    def SetVerboseObj(self, verbose_obj):
        self.m_verbose_obj=verbose_obj
    def Verbose(self, verbose, txt, start=False, end=False):
        if self.m_verbose_obj==None:
            return
        self.m_verbose_obj.Verbose(verbose, txt, start, end)
    def GetList(self):
        return self.m_build_list
    def Find(self, name):
        if name in self.m_build_dict:
            return self.m_build_dict[name]
        test=name+".default"
        if test in self.m_build_dict:
            return self.m_build_dict[test]
        return None
    def AddSearchPath(self, path):
        self.m_search_paths.append(path)
    def LoadAll(self):
        for path in self.m_search_paths:
            self.Load(path, "")
        return 0
    def Load(self, main_path, ref_name):
        # First the python files
        paths=glob.glob(os.path.join(main_path,"*.py"))
        for path in paths:
            self.LoadBuild(path, ref_name)
        # Second gets the folders, which can't have a "."
        paths=glob.glob(os.path.join(main_path,"*"))
        for path in paths:
            if os.path.isdir(path)==False:
                continue
            folder=os.path.split(path)[1]
            if folder.find("__")==0:
                continue
            name=""
            if (ref_name!="") and (ref_name!=None):
                name=ref_name+"."            
            name+=os.path.split(path)[1]
            self.Load(path, name)
        return 0
    def LoadBuild(self, path, ref_name):
        default=None
        result, build=Build.Load(path)
        if result<0:
            return result 
        if (ref_name=="") or (ref_name==None):
            name=build.GetName()
        else:
            name=ref_name+"."+build.GetName()
            if build.GetDefault()==True:
                default=ref_name+".default"
        build.SetFullName(name)
        if (name in self.m_build_dict)==True:
            self.Verbose(0, "ERROR: found duplicate build %s")
        else:
            self.m_build_dict[name]=build
            if default!=None:
                self.m_build_dict[default]=build
            self.m_build_list.append(build)
        return 0
