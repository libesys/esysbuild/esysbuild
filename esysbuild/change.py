## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

class Change:
    def __init__(self):
        self.m_gerrit=None
        self.m_json=None
        self.m_py=None
    def SetGerrit(self, gerrit):
        self.m_gerrit=gerrit
    def GetGerrit(self):
        return self.m_gerrit
    def SetJSON(self, json):
        self.m_json=json
    def GetJSON(self):
        return self.m_json
    def SetPy(self, py):
        self.m_py=py
    def GetPy(self):
        return self.m_py
    
    