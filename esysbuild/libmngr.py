## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import sys

from esysbuild.lib import *

class LibMngr(object):
    def __init__(self, name=None, **kwds):
        super().__init__(**kwds)
        self.m_lib_list=[]
        self.m_lib_dict={}
        self.m_mngr_list=[]
        self.m_mngr_dict={}
        self.m_name=name
        self.m_verbose_obj=None        
    def SetVerboseObj(self, verbose_obj):
        self.m_verbose_obj=verbose_obj
    def Verbose(self, verbose, txt, start=False, end=False):
        if self.m_verbose_obj==None:
            return
        self.m_verbose_obj.Verbose(verbose, txt, start, end)
    def SetName(self, name):
        self.m_name=name
    def GetName(self):
        return self.m_name
    def Add(self, lib):
        self.m_lib_list.append(lib)
        self.m_lib_dict[lib.GetName()]=lib
    def AddMngr(self, mngr):
        self.m_mngr_list.append(mngr)
        self.m_mngr_dict[mngr.GetName()]=mngr
    def GetCount(self):
        return len(self.m_lib_list)
    def Get(self, idx):
        if idx>=self.GetCount():
            return None
        return self.m_lib_list[idx]
    def Find(self, name):
        if name in self.m_lib_dict:
            return self.m_lib_dict[name]
        return None
    def GetLibList(self):
        return self.m_lib_list


