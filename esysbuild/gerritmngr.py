## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

from esysbuild.gerrit import *

class GerritMngr:
    def __init__(self):
        self.m_gerrit_list=[]
        self.m_gerrit_dict={}
        self.m_verbose_obj=None
    def SetVerboseObj(self, verbose_obj):
        self.m_verbose_obj=verbose_obj
    def Verbose(self, verbose, txt, start=False, end=False):
        if self.m_verbose_obj==None:
            return
        self.m_verbose_obj.Verbose(verbose, txt, start, end)
    def Add(self, name, url, port=29418):
        gerrit=Gerrit(name, url, port)
        self.m_gerrit_list.append(gerrit)
        self.m_gerrit_dict[name]=gerrit
    def GetList(self):
        return self.m_gerrit_list
    def Find(self, name):
        if (name in self.m_gerrit_dict)==False:
            return None
        return self.m_gerrit_dict[name]
    def GetChanges(self, topic, only_open=True):
        changes=[]
        for gerrit in self.GetList():
            temp=gerrit.GetChanges(topic, only_open)            
            if temp!=None:
                self.Verbose(2, "found %i changes" % len(temp))
                changes+=temp
        return changes
    