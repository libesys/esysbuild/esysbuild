import argparse
import jinja2
import os


class CreateISS:
    def __init__(self) -> None:
        self.m_args = None
        self.m_name = None
        self.m_base_path = None
        self.m_script_path = None
        self.m_search_path = None
        self.m_sign = False
        self.m_output_folder = None
        self.m_template_file = None

    def _get_abs_path(self, path):
        if path is None:
            return None
        if os.path.isabs(path):
            return path
        full_path = path
        if self.get_base_path() is not None:
            full_path = os.path.join(self.get_base_path(), path)
            full_path = os.path.normpath(full_path)
        return os.path.abspath(full_path)

    def set_base_path(self, base_path):
        self.m_base_path = base_path

    def get_base_path(self):
        return self.m_base_path

    def set_template_file(self, template_file):
        self.m_template_file = template_file
        name = os.path.splitext(os.path.basename(template_file))[0]
        self.set_name(name)
        self._set_search_path(os.path.dirname(template_file))

    def get_template_file(self, abs=False):
        if not abs:
            return self.m_template_file
        return self._get_abs_path(self.m_template_file)

    def set_output_folder(self, output_folder):
        self.m_output_folder = output_folder

    def get_output_folder(self, abs=False):
        if not abs:
            return self.m_output_folder
        return self._get_abs_path(self.m_output_folder)

    def set_name(self, name):
        self.m_name = name

    def get_name(self):
        return self.m_name

    def set_args(self, args):
        self.m_args = args

    def get_args(self):
        return self.m_args

    def set_script_path(self, script_path):
        path = os.path.dirname(script_path)
        self.set_script_folder(path)
        self.set_base_path(path)

    def set_script_folder(self, script_folder):
        self.m_script_folder = script_folder

    def get_script_folder(self, abs=False):
        if not abs:
            return self.m_script_folder
        return self._get_abs_path(self.m_script_folder)

    def _set_search_path(self, search_path):
        self.m_search_path = search_path

    def _get_search_path(self):
        return self.m_search_path

    def set_sign(self, sign):
        self.m_sign = sign

    def get_sign(self):
        return self.m_sign

    def handle_cmd_line(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "--name",
            dest="name",
            action="store",
            default=None,
            required=True,
            help="base filename",
        )
        parser.add_argument(
            "--sign",
            dest="sign",
            action="store_true",
            default=False,
            help="create a ISS to sign all files",
        )
        args = parser.parse_args()
        self.set_args(args)
        self.set_name(args.name)
        if args.sign:
            self.set_sign(True)

    def do(self):
        if self.get_template_file() is None:
             return 0
        input_path = self._get_abs_path(self._get_search_path())
        output_folder = self.get_output_folder(True)
        if output_folder is None:
            output_folder = input_path
        
        template_file = self.get_name() + ".j2"
        output_file = self.get_name() + ".iss"

        print("Template file : %s" % template_file)
        print("Output file   : %s" % output_file)

        loader = jinja2.FileSystemLoader(searchpath=input_path)
        env = jinja2.Environment(loader=loader)
        env.variable_start_string = "[["
        env.variable_end_string = "]]"
        env.block_start_string = "[%"
        env.block_end_string = "%]"
        env.comment_start_string = "[#"
        env.comment_end_string = "#]"

        template = env.get_template(template_file)

        if self.get_sign():
            setup_sign_tool = "SignTool=default"
            define_sign = '#define SIGN "sign"'
        else:
            setup_sign_tool = ""
            define_sign = '#define SIGN ""'

        data = {
            "define_sign": define_sign,
            "setup_sign_tool": setup_sign_tool,
        }

        # this is where to put args to the template renderer
        output_text = template.render(data)

        # print(output_text)

        output_folder = os.path.join(output_folder, output_file)
        with open(output_folder, "w", encoding="utf-8") as f:
            f.write(output_text)

        return 0


if __name__ == "__main__":
    print("Creating ISS file ...")

    create_iss = CreateISS()
    create_iss.set_script_path(__file__)
    create_iss.handle_cmd_line()

    result = create_iss.do()
    if result == 0:
        print("Creating ISS file done.")
    else:
        print("Error creating ISS file.")
    exit(-result)
