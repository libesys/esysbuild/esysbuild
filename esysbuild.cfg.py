## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

from esysbuild.lib import *
from esysbuild.buildfile import *

class ESysBuildCfgFile(BuildFile):
    def __init__(self):
        super().__init__()
    def AddLibs(self, lib_mngr):                  
        esysbuild=Lib("main")               
        lib_mngr.Add(esysbuild)               
        return 0
    
def GetBuildFile():
    return ESysBuildCfgFile()



    