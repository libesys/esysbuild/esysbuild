import os
import sys

add_search_path=os.path.abspath(globals()["__file__"])
add_search_path=os.path.split(add_search_path)[0]

from srctreeitem import *
from srctree import *

sys.path.append(add_search_path)

def Test():
    esys_tree=SrcTree("src.esys.esys")
    src=esys_tree.AddFolder("src")
    ard=src.AddFolder("arduino")
    evtloop=src.AddFolder("evtloop")
    evtloop.AddFile("task_evtloop.cpp")
    evtloop.AddFile("taskbase_evtloop.cpp")
    evtloop.AddFile("tasklet_evtloop.cpp")
    evtloop.AddFile("taskmngr_evtloop.cpp")
    evtloop.AddFile("taskmngrbase_evtloop.cpp")
    evtloop.AddFile("taskplatif_evtloop.cpp")
    evtloop_ard=evtloop.AddFolder("arduino")
    evtloop_ard.AddFile("esys_ard_evtloop.cpp")
    evtloop_ard.AddFile("logger_evtloop.cpp")
    evtloop_ard.AddFile("task_ard_evtloop.cpp")
    src.AddFile("buffer.cpp")
    src.AddFile("busmngrbase.cpp")
    src.AddFile("connection.cpp")
    
    inc=esys_tree.AddFolder("include")
    



if __name__ == "__main__":
    Test()





