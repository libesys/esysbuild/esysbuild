## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import ebxml

class CDT4Gen:
    VirtualFolder=0
    LinkToFolder=1
    LinkToFile=2

    def __init__(self):
        self.m_ebxml=ebxml.Writer()
        self.m_src_linked_res=[]
        self.m_natures_list=[]
        self.m_natures_dict={}
        self.m_support_virtual_folders=True
        self.m_support_gmake_error_parser=True
        self.m_support_macho64parser=True
        self.m_c_enabled=False
        self.m_cxx_enabled=False
        self.m_eclipse_version=None
        self.m_output_dir=None
        self.m_project_name=None
    def SetOutputDir(self, output_dir):
        self.m_output_dir=output_dir
    def GetOutputDir(self):
        return self.m_output_dir
    def SetProjectName(self, project_name):
        self.m_project_name=project_name
    def GetProjectName(self):
        return self.m_project_name
    def SetEclipseVersion(self, version):
        #version should be given as a string "Major.Minor"
        self.m_eclipse_version=version
        i=version.find(".")
        if i==-1:
            ver_number=int(version)*1000
        else:
            ver_number=int(version[:i])*1000+int(version[i+1:])
        if ver_number<3006:     # 3.6 Helios
            self.m_support_virtual_folders=False
            self.m_support_macho64parser=False
        elif ver_number<3007:   # 3.7 Indigo
            self.m_support_gmake_error_parser=False
    def AddNature(self, nature):
        if self.m_natures_dict.has_key(nature)==False:
            self.m_natures_list.append(nature)
            self.m_natures_dict[nature]=True
    def AppendAttrib(self, keyval):
        self.m_ebxml.StartElement("attribute")
        self.m_ebxml.Attribute("key", keyval)
        self.m_ebxml.Attribute("value", keyval)
        self.m_ebxml.EndElement()
    def AppendDict(self, key, value):
        self.m_ebxml.StartElement("dictionary")
        self.m_ebxml.Element("key", key)
        self.m_ebxml.Element("value", value)
        self.m_ebxml.EndElement()
    def SetCEnabled(self):
        self.m_c_enabled=True
        self.AddNature("org.eclipse.cdt.core.cnature")
    def SetCXXEnabled(self):
        self.m_c_enabled=True
        self.AddNature("org.eclipse.cdt.core.ccnature")
        self.AddNature("org.eclipse.cdt.core.cnature")
    def Generate(self):
        # self.CreateSourceProjectFile() # probably don't need that

        # create a .project file
        self.CreateProjectFile()
        # create a .cproject file
        self.CreateCProjectFile()
    def AddEnvVar(self):
        pass
    def CreateProjectFile(self):
        self.m_ebxml.SetFileName(os.path.join(self.GetOutputDir(),".project"))
        self.m_ebxml.StartDocument("UTF-8")
        self.m_ebxml.StartElement("projectDescription")
        self.m_ebxml.Element("name", self.GetProjectName())

        self.m_ebxml.Element("comment", "")
        self.m_ebxml.Element("projects", "")

        self.m_ebxml.StartElement("buildSpec")
        self.m_ebxml.StartElement("buildCommand")
        self.m_ebxml.Element("name", "org.eclipse.cdt.managedbuilder.core.genmakebuilder")
        self.m_ebxml.Element("triggers", "clean,full,incremental,")
        self.m_ebxml.StartElement("arguments")
        # should add all calls to AppendDict here
        self.m_ebxml.EndElement();  # arguments
        self.m_ebxml.EndElement();  # buildCommand

        self.m_ebxml.StartElement("buildSpec")
        self.m_ebxml.StartElement("buildCommand")
        self.m_ebxml.Element("name", "org.eclipse.cdt.managedbuilder.core.ScannerConfigBuilder")
        self.m_ebxml.Element("triggers", "clean,full,incremental,")
        self.m_ebxml.StartElement("arguments")
        # should add all calls to AppendDict here
        self.m_ebxml.EndElement();  # arguments
        self.m_ebxml.EndElement();  # buildCommand
        self.m_ebxml.EndElement();  # buildSpec

        self.m_ebxml.StartElement("natures");
        for nature in self.m_natures_list:
            self.m_ebxml.Element("nature", nature)
        self.m_ebxml.Element("nature", "org.eclipse.cdt.managedbuilder.core.managedBuildNature")
        self.m_ebxml.Element("nature", "org.eclipse.cdt.managedbuilder.core.ScannerConfigNature")

        self.m_ebxml.EndElement()   # natures

        self.m_ebxml.StartElement("linkedResources")
        # We assume that we always do build out of source, in source is nasty

        if self.m_support_virtual_folders:
            self.CreateLinksToSubprojects(self.GetOutputDir())
            self.CreateLinksForTargets()

        self.m_ebxml.EndElement()   # linkedResources
        self.m_ebxml.EndElement()   # projectDescription

    def WriteGroups(self):
        pass
    def CreateLinksForTargets(self):
        link_name="[Targets]"
        self.AppendLinkedResource(link_name, "virtual:/virtual", self.VirtualFolder)
        pass
    def CreateLinksToSubprojects(self, path):
        pass
    def AppendIncludeDirectories(self):
        pass
    def CreateCProjectFile(self):
        pass
    def GetEclipsePath(self):
        pass
    def GetPathBasename(self, path):
        pass
    def AppendStorageScanners(self):
        pass
    def AppendTarget(self):
        pass
    def AppendScannerProfile(self):
        pass
    def AppendLinkedResource(self, name, path, link_type):
        loc_tag="location"
        type_tag=2
        if link_type==self.VirtualFolder:
            loc_tag="locationURI"
        elif link_type==self.LinkToFile:
            type_tag=1

        self.m_ebxml.StartElement("link")
        self.m_ebxml.Element("name", name);
        self.m_ebxml.Element("type", type_tag);
        self.m_ebxml.Element(loc_tag, path);
        self.m_ebxml.EndElement();
