import os

from srctreeitem import *


class SrcTree(SrcTreeItem):
    def __init__(self, name):
        SrcTreeItem.__init__(self)
        self.m_name=name
        self.m_top=None
    def AddFolder(self, name):
        folder=SrcTreeItem.AddFolder(name)
        folder.SetTree(self)
    def AddFile(self, name):
        file=SrcTreeItem.AddFile(name)
        file.SetTree(self)



