#!/usr/bin/env python3

## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

import os
import sys
import argparse
import subprocess
import cmd
import glob
import filecmp
import shutil
import platform
import stat
import inspect
import importlib.util
from _ast import If

#from esysbuild.libmngr import *
#from esysbuild.lib import *

script_path=sys.argv[0]
sys_path=os.path.split(script_path)[0]
sys.path.append(sys_path)   # By default adds folder where the script is located to 

##XESYSBUILD<file name="esysbuild.dynmod">
##XESYSBUILD<content>
### 
### __legal_b__
###
### Copyright (c) 2017 Michel Gillet
### Distributed under the wxWindows Library Licence, Version 3.1.
### (See accompanying file LICENSE_3_1.txt or
### copy at http://www.wxwidgets.org/about/licence)
###
### __legal_e__
###
#import os
#import sys
#import importlib.util
#from esysbuild.dynmod import *
##XESYSBUILD</content>

class DynMod(object):
    FILE_NO_EXISTING=-1
    SYNTAX_ERROR=-2
    GET_FCT_NOT_FOUND=-3
    def __init__(self, **kwds):
        super().__init__(**kwds)
        self.m_path=None
        self.m_module=None
        self.m_module_name=None
    def SetPath(self, path):
        self.m_path=path
    def GetPath(self):
        return self.m_path
    def SetModule(self, module):
        self.m_module=module
    def GetModule(self):
        return self.m_module
    def SetModuleName(self, module_name):
        self.m_module_name= module_name
    def GetModuleName(self):
        return self.m_module_name
    @staticmethod
    def LoadModulePy34(path, mod_name):
        import importlib.machinery
        try:
            module = importlib.machinery.SourceFileLoader(mod_name,path).load_module()
        except SyntaxError as ex:
            print("ERROR: config file %s" % path)
            print("    %s:" % ex)
            print("    %s" % ex.text.lstrip()) 
            return [DynMod.SYNTAX_ERROR,None]
        return [0, module]
    @staticmethod
    def LoadModulePy35(path, mod_name):
        spec = importlib.util.spec_from_file_location(mod_name, path)
        module = importlib.util.module_from_spec(spec)
        try:
            spec.loader.exec_module(module)
        except SyntaxError as ex:
            print("ERROR: config file %s" % path)
            print("    %s:" % ex)
            print("    %s" % ex.text.lstrip()) 
            return [DynMod.SYNTAX_ERROR,None]
        return [0, module]
    @staticmethod
    def LoadModule(path, mod_name):
        if sys.version_info.minor==4:
            return DynMod.LoadModulePy34(path, mod_name)
        elif sys.version_info.minor>=5:
            return DynMod.LoadModulePy35(path, mod_name)    
    @staticmethod        
    def Load(path, mod_name, fct_name, fct_params=None):
        if os.path.exists(path)==False:
            return [DynMod.FILE_NO_EXISTING,None]        
        result, module=DynMod.LoadModule(path, mod_name)
        if result<0:
            return [result, module]
        if fct_name in module.__dict__:
            try:
                if fct_params==None:
                    dyn_mod_file=module.__dict__[fct_name]()
                else:                
                    dyn_mod_file=module.__dict__[fct_name](*fct_params)
            except TypeError as ex:
                print("ERROR: config file %s" % path)
                print("    %s:" % ex)
                return [DynMod.SYNTAX_ERROR,None]
            except SyntaxError as ex:
                print("ERROR: config file %s" % path)
                print("    %s:" % ex)
                print("    %s" % ex.text.lstrip()) 
                return [DynMod.SYNTAX_ERROR,None]
            dyn_mod_file.SetModule(module)
            dyn_mod_file.SetPath(path)
            return [0,dyn_mod_file]
        else:
            return [DynMod.GET_FCT_NOT_FOUND,None]
        
##XESYSBUILD</file>

##XESYSBUILD<file name="esysbuild.cfgfile">
##XESYSBUILD<content>
### 
### __legal_b__
###
### Copyright (c) 2017 Michel Gillet
### Distributed under the wxWindows Library Licence, Version 3.1.
### (See accompanying file LICENSE_3_1.txt or
### copy at http://www.wxwidgets.org/about/licence)
###
### __legal_e__
###
#import os
#from esysbuild.dynmod import *
##XESYSBUILD</content>

class CfgFile(DynMod):
    ROOT=0
    BUILD=1    
    def __init__(self, typ=None, **kwds):
        super().__init__(**kwds)
        if typ==None:
            self.m_type=CfgFile.ROOT
        else:
            self.m_type=typ
        self.m_in_dev_folder=False
        self.m_esysbuild_src_dir="esysbuild"    #default in subfolder of CfgFile, always relative path
        self.m_folder_path=None
        self.m_file_name=None
        self.m_build_name=None
    def SetPath(self, path):
        super().SetPath(path)
        folder_path=os.path.split(path)[0]
        self.SetFolderPath(folder_path)
    def SetType(self, typ):
        self.m_type=typ
    def GetType(self):
        return self.m_type
    def SetInDevFolder(self, in_dev_folder):
        self.m_in_dev_folder=in_dev_folder
    def GetInDevFolder(self):
        return self.m_in_dev_folder
    def SetESysBuildSrcDir(self, esysbuild_src_dir):
        self.m_esysbuild_src_dir=esysbuild_src_dir
    def GetESysBuildSrcDir(self):
        return self.m_esysbuild_src_dir
    def SetFolderPath(self, folder_path):
        self.m_folder_path=folder_path
    def GetFolderPath(self):
        return self.m_folder_path
    def SetFileName(self, filename):
        self.m_file_name=filename
    def GetFileName(self):
        return self.m_file_name
    def SetBuildName(self, build_name):
        self.m_build_name=build_name
    def GetBuildName(self):
        return self.m_build_name
    def Write(self, folder_path, filename="config.py", class_name="TheCfgFile"):
        path=os.path.join(folder_path, filename)
        self.SetFolderPath(folder_path)
        self.SetFileName(filename)
        fout=open(path, "wt")
        
        fout.write("def GetCfgFile(cfg_file, CfgFile):\n")
        fout.write("    cfg_file.SetType(")
        if self.GetType()==CfgFile.ROOT:
            fout.write("CfgFile.ROOT")
        elif self.GetType()==CfgFile.BUILD:
            fout.write("CfgFile.BUILD")
        else:
            fout.close()
            return -1
        fout.write(")\n")
        fout.write("    cfg_file.SetInDevFolder(%s)\n" % self.GetInDevFolder())
        fout.write("    cfg_file.SetESysBuildSrcDir(\"%s\")\n" % self.GetESysBuildSrcDir())
        fout.write("    cfg_file.SetFolderPath(\"%s\")\n" % self.GetFolderPath())
        fout.write("    cfg_file.SetFileName(\"%s\")\n" % self.GetFileName())
        if self.GetType()==CfgFile.BUILD:
            fout.write("    cfg_file.SetBuildName(\"%s\")\n" % self.GetBuildName()) 
        fout.write("    return cfg_file\n")
        fout.close()
        return 0
    
        fout.write("from esysbuild.cfgfile import *\n\n")
        fout.write("class "+class_name+"(CfgFile):\n")
        fout.write("    def __init__(self):\n")
        fout.write("        super().__init__()\n")
        fout.write("        self.SetType(")
        if self.GetType()==CfgFile.ROOT:
            fout.write("CfgFile.ROOT")
        elif self.GetType()==CfgFile.BUILD:
            fout.write("CfgFile.BUILD")
        else:
            fout.close()
            return -1
        fout.write(")\n")        
        fout.write("        self.SetInDevFolder(%s)\n" % self.GetInDevFolder())
        fout.write("        self.SetESysBuildSrcDir(\"%s\")\n" % self.GetESysBuildSrcDir())
        fout.write("        self.SetFolderPath(\"%s\")\n" % self.GetFolderPath())
        fout.write("        self.SetFileName(\"%s\")\n" % self.GetFileName())
        if self.GetType()==CfgFile.BUILD:
            fout.write("        self.SetBuildName(\"%s\")\n" % self.GetBuildName())
        fout.write("\n")
        fout.write("def %s():\n" % "GetCfgFile")
        fout.write("    return %s()\n" % class_name)
        fout.close()
        return 0
    def CopyToBuildType(self, path, build_name):
        if self.GetType()!=CfgFile.ROOT:
            return None        
        cfg_file=CfgFile()
        cfg_file.SetType(CfgFile.BUILD)
        cfg_file.SetFolderPath(path)
        cfg_file.SetBuildName(build_name)
        cfg_file.SetInDevFolder(self.GetInDevFolder())

        orig_src_dir=os.path.join(self.GetFolderPath(), self.GetESysBuildSrcDir())
        orig_src_dir=os.path.normpath(orig_src_dir)        
        new_src_dir=os.path.relpath(orig_src_dir, path)
        cfg_file.SetESysBuildSrcDir(new_src_dir)        
        return cfg_file
        
    @staticmethod        
    def Load(path, name="config"):
        return DynMod.Load(path, "config", "GetCfgFile", [CfgFile(), CfgFile])
    
##XESYSBUILD</file>

##XESYSBUILD<file name="esysbuild.groupmngrbase">   
##XESYSBUILD<content>
##XESYSBUILD</content>
## 
## __legal_b__
##
## Copyright (c) 2017 Michel Gillet
## Distributed under the wxWindows Library Licence, Version 3.1.
## (See accompanying file LICENSE_3_1.txt or
## copy at http://www.wxwidgets.org/about/licence)
##
## __legal_e__
##

class GroupMngrBase(object):
    def __init__(self,**kwds):
        super().__init__(**kwds)
        self.m_group_names_list=[]
        self.m_group_names_dict={}
    def AddGroupName(self, group_name):
        typ_=type(group_name)
        if typ_==str:
            self.m_group_names_dict[group_name]=None
            self.m_group_names_list.append(group_name)
        elif typ_==list:
            for name in group_name:
                if name in self.m_group_names_dict:
                    continue
                self.m_group_names_dict[name]=None
                self.m_group_names_list.append(name)
        else:
            pass
    def GetGroupDict(self):
        return self.m_group_names_dict
    def GetGroupList(self):
        return self.m_group_names_list                    
    def GetGroupCount(self):
        return len(self.m_group_names_list)
    def GetGroupName(self, idx):
        if idx>=self.GetGroupCount():
            return None
        return self.m_group_names_list[idx]
    def GetGroup(self, idx):
        if idx>=self.GetGroupCount():
            return None
        return self.m_group_names_list[idx]
    def Intersection(self, group_mngr):
        if type(group_mngr)==GroupMngrBase:
            the_list=group_mngr.GetGroupList()
        elif type(group_mngr)==list:
            the_list=group_mngr
        result=set(self.m_group_names_list).intersection(the_list)
        return list(result)

##XESYSBUILD</file>

##XESYSBUILD<file name="esysbuild.buildfile">   
##XESYSBUILD<content>
### 
### __legal_b__
###
### Copyright (c) 2017 Michel Gillet
### Distributed under the wxWindows Library Licence, Version 3.1.
### (See accompanying file LICENSE_3_1.txt or
### copy at http://www.wxwidgets.org/about/licence)
###
### __legal_e__
###
#from esysbuild.dynmod import *
##XESYSBUILD</content>
 
class BuildFile(DynMod):
    def __init__(self, **kwds):
        super().__init__(**kwds)
        self.m_boards_rel_dir=None
        self.m_builds_rel_dir=None
        self.m_bootstrap_script=None
    def AddGerrits(self, gerrit_mgr):
        return 0
    def AddLibs(self, lib_mngr):
        return 0    
    def SetBoardsRelDir(self, boards_rel_dir):
        self.m_boards_rel_dir=os.path.normpath(boards_rel_dir)
    def GetBoardsRelDir(self):
        return self.m_boards_rel_dir
    def SetBuildsRelDir(self, builds_rel_dir):
        self.m_builds_rel_dir=os.path.normpath(builds_rel_dir)
    def GetBuildsRelDir(self):
        return self.m_builds_rel_dir
    def GetBuildsDir(self):
        path=os.path.split(self.GetPath())[0]
        return os.path.join(path,self.GetBuildsRelDir())
    def GetBoardsDir(self):
        path=os.path.split(self.GetPath())[0]
        return os.path.join(path,self.GetBoardsRelDir())
    def SetBootStrapScript(self, bootstrap_script):
        self.m_bootstrap_script=bootstrap_script
    def GetBootStrapScript(self):
        return self.m_bootstrap_script
    
    
    @staticmethod        
    def Load(path, name="esysbuild.cfg"):
        return DynMod.Load(path, "esysbuild.cfg", "GetBuildFile")        

##XESYSBUILD</file>


class Board:
    def __init__(self, name, host, cc, cxx, cross_compile=True):
        self.m_name=name
        self.m_host=host
        self.m_cc=cc
        self.m_cxx=cxx
        self.m_cflags=None
        self.m_cxxflags=None
        self.m_cross_compile=cross_compile
    def GetName(self):
        return self.m_name
    def GetHost(self):
        return self.m_host
    def GetCC(self):
        return self.m_cc
    def GetCXX(self):
        return self.m_cxx
    def SetCFlags(self, cflags):
        self.m_cflags=cflags
    def SetCXXFlags(self, cxxflags):
        self.m_cxxflags=cxxflags
    def GetCFlags(self):
        return self.m_cflags
    def GetCXXFlags(self):
        return self.m_cxxflags
    def GetCrossCompile(self):
        return self.m_cross_compile
    
class BoardMngr:
    def __init__(self):
        self.m_board_dict={}
        self.m_board_list=[]
    def Register(self, board):
        if board.GetName() in self.m_board_dict:
            print("ERROR: board with name %s was already registered" % board.GetName())
            return -1 
        self.m_board_dict[board.GetName()]=board
        self.m_board_list.append(board)
    def Find(self, name):
        if name in self.m_boards:
            return self.m_boards[name]
        return None
    def GetCount(self):
        return len(self.m_board_list)
    def GetBoardName(self, idx):
        if idx>=len(self.m_board_list):
            return None
        return self.m_board_list[idx].GetName()
    def GetBoard(self, idx):
        if idx>=len(self.m_board_list):
            return None
        return self.m_board_list[idx]
    def GetList(self):
        return self.m_board_list
    def GetDict(self):
        return self.m_board_dict
        

def InitBoards(board_mngr):
    vhw=Board("vhw", "", "gcc", "gcc", False)
    #vhw.SetCFlags("-mthumb -mcpu=cortex-m0plus --specs=nosys.specs")
    #vhw.SetCXXFlags("-mthumb -mcpu=cortex-m0plus --specs=nano.specs")
    board_mngr.Register(vhw)

    ard_m0_pro=Board("ard_m0_pro", "arm-none-eabi", "arm-none-eabi-gcc", "arm-none-eabi-gcc")
    ard_m0_pro.SetCFlags("-mthumb -mcpu=cortex-m0plus --specs=nosys.specs")
    ard_m0_pro.SetCXXFlags("-mthumb -mcpu=cortex-m0plus --specs=nano.specs")
    board_mngr.Register(ard_m0_pro)


class ConfFile:
    def __init__(self, name=None):
        self.m_name=name
    def GetName(self):
        return self.m_name
    def SetName(self, name):
        self.m_name=name

class ESysBuild:
    def __init__(self):
        self.m_script_path=None         # Full path to esysbuild script which started this
        self.m_build=None
        self.m_host=None
        self.m_configure_root=None
        self.m_config_output_dir=None
        self.m_board_name=None
        self.m_cc=None
        self.m_cxx=None        
        self.m_compiler_bin_path=None
        self.m_folders=None
        self.m_fout=None
        self.m_is_windows=(platform.system().lower().find("win") != -1)
        self.m_is_cygwin=(platform.system().lower().find("cygwin") != -1)
        if ( (platform.system().lower().find("linux") != -1) or
             (platform.system().lower().find("msys") != -1) ):
            self.m_is_linux=True
        else:
            self.m_is_linux=False        
        self.m_in_dev_folder=False
        self.m_repo_root_dir=None   # the folder where .repo is found
        self.m_verbose=0
        self.m_verbose_depth=0
        self.m_verbose_depth_str="    "
        self.m_verbose_on=True
        self.m_build_dir=None
        self.m_cfg_file=None
        self.m_cfg_file_load_result=None
        self.m_build_cfg_file=None
        self.m_lib_mngr=None
        self.m_repo_mngr=None
        self.m_board_mngr=BoardMngr()
        self.m_src_dir=None             # The directory where the source of ESysBuild are found
        self.m_ld_flags=None
        self.m_compiler_mngr=None
        self.m_esysbuildsdk=None
        self.m_build_mngr=None
        self.m_build_file=None
        self.m_build_name=None
        self.m_gerrit_mngr=None
        self.m_dry_run=False
        self.m_job_count=None
        self.m_cxx_ver=None
        self.m_debug_build=True
        self.m_release_build=False
        self.m_cmake_build=False
        self.m_cmake=None
        if ("VERBOSE" in os.environ)==True:
            value=int(os.environ["VERBOSE"])
            self.SetVerbose(value)
    def SetJobCount(self, job_count):
        self.m_job_count=job_count
    def GetJobCount(self):
        return self.m_job_count
    def SetCxxVer(self, cxx_ver):
        self.m_cxx_ver=cxx_ver
    def GetCxxVer(self):
        return self.m_cxx_ver 
    def SetDebugBuild(self):
        self.m_debug_build=True
        self.m_release_build=False
    def GetDebugBuild(self):
        return self.m_debug_build
    def SetReleaseBuild(self):
        self.m_debug_build=False
        self.m_release_build=True
    def GetReleaseBuild(self):
        return self.m_release_build
    def SetDryRun(self, dry_run=True):
        self.m_dry_run=dry_run
    def GetDryRun(self):
        return self.m_dry_run
    def SetBuildFile(self, build_file):
        self.m_build_file=build_file
    def GetBuildFile(self):
        return self.m_build_file
    def SetCfgFile(self, cfg_file):
        self.m_cfg_file=cfg_file
    def GetCfgFile(self):
        return self.m_cfg_file
    def SetCfgFileLoadResult(self, result):
        self.m_cfg_file_load_result=result
    def GetCfgFileLoadResult(self):
        return self.m_cfg_file_load_result
    def SetLDFlags(self, ld_flags):
        self.m_ld_flags=ld_flags
    def GetLDFlags(self):
        return self.m_ld_flags
    def SetSrcDir(self, src_dir):
        self.m_src_dir=src_dir
    def GetSrcDir(self):
        return self.m_src_dir
    def SetBuildDir(self, build_dir):
        self.m_build_dir=build_dir
    def GetBuildDir(self):
        return self.m_build_dir    
    def GetBoardMngr(self):
        return self.m_board_mngr
    def GetLibMngr(self):
        if self.m_lib_mngr==None:
            import esysbuild.libmngr
            self.m_lib_mngr=esysbuild.libmngr.LibMngr()
        return self.m_lib_mngr
    def GetCompilerMngr(self):
        if self.m_compiler_mngr==None:
            import esysbuild.compilermngr
            self.m_compiler_mngr=esysbuild.compilermngr.CompilerMngr()
        return self.m_compiler_mngr
    def GetBuildMngr(self):
        if self.m_build_mngr==None:
            import esysbuild.buildmngr
            self.m_build_mngr=esysbuild.buildmngr.BuildMngr()
        return self.m_build_mngr
    def GetRepoMngr(self):
        if self.m_fout!=None:
            self.m_fout.write("GetRepoMngr\n")
            self.m_fout.write("sys.path = %s\n" % sys.path)
        if self.m_repo_mngr==None:                            
            import esysbuild.repomngr
            self.m_repo_mngr=esysbuild.repomngr.RepoMngr()
        return self.m_repo_mngr
    def GetGerritMngr(self):
        if self.m_gerrit_mngr==None:
            import esysbuild.gerritmngr
            self.m_gerrit_mngr=esysbuild.gerritmngr.GerritMngr()
        return self.m_gerrit_mngr
    def SetRepoRootDir(self, repo_root_dir):
        self.m_repo_root_dir=repo_root_dir
    def GetRepoRootDir(self):
        return self.m_repo_root_dir    
    def GetScriptPath(self):
        return self.m_script_path
    def GetScriptFolder(self):
        path=self.GetScriptPath()
        if path==None:
            return None
        path=os.path.split(path)[0]
        return path
    def SetScriptPath(self, script_path):
        self.LogCall(2)
        self.m_script_path=script_path
    def IsWindow(self):
        return self.m_is_windows
    def IsCygwin(self):
        return self.m_is_cygwin
    def IsLinux(self):
        return self.m_is_linux
    def SetVerbose(self, verbose):
        self.m_verbose=verbose
    def GetVerbose(self):
        return self.m_verbose
    def SetVerboseOn(self, verbose_on=True):
        self.m_verbose_on=verbose_on
    def SetVerboseOff(self):
        self.m_verbose_on=False
    def GetVerboseOn(self):
        return self.m_verbose_on
    def IncVerboseDepth(self, value=1):
        self.m_verbose_depth+=value
    def DecVerboseDepth(self, value=1):
        self.m_verbose_depth-=value
    def LogCall(self, verbose):
        if verbose<=self.GetVerbose() and self.GetVerboseOn()==True:
            s=""
            for idx in range(self.m_verbose_depth):
                s+=self.m_verbose_depth_str
            frame=inspect.stack()[1].frame            
            idx=0
            txt=""
            class_name=None
            if ("self" in frame.f_locals)==True:
                class_name=frame.f_locals["self"].__class__.__name__
                txt=class_name+"."            
            txt+=frame.f_code.co_name+"("
            for arg in frame.f_locals.items():                
                if idx!=0:
                    txt+=", "
                if arg[0]!="self":
                    txt+=arg[0]+(" = %s" % arg[1])
                    idx=idx+1
            txt+=")"
            print(s+txt)
    def Verbose(self, verbose, txt, start=False, end=False):
        if verbose<=self.GetVerbose() and self.GetVerboseOn()==True:
            if end==True:
                self.m_verbose_depth-=1
                if self.m_verbose_depth<0:
                    self.m_verbose_depth=0
            s=""
            for idx in range(self.m_verbose_depth):
                s+=self.m_verbose_depth_str
            print(s+txt)
            if start==True:
                self.m_verbose_depth+=1
    def Close(self):
        if self.m_fout!=None:
            self.m_fout.close()
    def SetFolders(self, folders):
        self.m_folders=folders
    def GetFolders(self):
        return self.m_folders
    def IsFolderSet(self):
        if self.m_folders==None:
            return False
        if self.m_folders==[]:
            return False
        if len(self.m_folders)==0:
            return False
        if len(self.m_folders)!=0:
            return True
        return False
    def SetHost(self, host):
        self.m_host=host
    def GetHost(self):
        return self.m_host
    def SetConfigureRoot(self, configure_root):
        self.m_configure_root=configure_root
    def GetConfigureRoot(self):
        return self.m_configure_root
    def SetOutputDir(self, output_dir):
        self.m_config_output_dir=output_dir
    def GetOutputDir(self):
        return self.m_config_output_dir
    def SetBuildName(self, build_name, set_out_dir=False):
        self.m_build_name=build_name
        #self.m_board=self.m_board_mngr.Find(build_name)
        result=self.LoadAllBuilds()
        if result<0:
            self.Verbose(0, "ERROR: couldn't load the builds")
            return -1
        build=self.GetBuildMngr().Find(build_name)
        self.SetBuild(build)
        if build!=None:
            pass
            #self.SetHost(self.m_board.GetHost())
            #self.SetCC(self.m_board.GetCC())
            #self.SetCXX(self.m_board.GetCXX())
        if set_out_dir==True:
            build_name=self.GetBuildName().replace(".", os.sep)
            output_dir=os.path.join(self.GetBuildDir(),build_name)
            if os.path.exists(output_dir)==False:
                os.makedirs(output_dir)
            self.SetOutputDir(output_dir)
    def GetBuildName(self):
        return self.m_build_name
    def SetBuild(self, build_):
        self.m_build=build_
    def GetBuild(self):
        return self.m_build
    def SetCC(self, cc):
        self.m_cc=cc
    def GetCC(self, full_path=False):
        if full_path==False:
            return self.m_cc
        return self.GetCompilerBinPath()+os.sep+self.m_cc
    def SetCXX(self, cxx):
        self.m_cxx=cxx
    def GetCXX(self, full_path=False):
        if full_path==False:
            return self.m_cxx
        return self.GetCompilerBinPath()+os.sep+self.m_cxx
    def SetCompilerBinPath(self, compiler_bin_path):
        self.m_compiler_bin_path=compiler_bin_path
    def GetCompilerBinPath(self):
        return self.m_compiler_bin_path
    def DetectBuild(self):
        DEVNULL = open(os.devnull, 'wb')
        cmd="gcc -print-multiarch"
        try:
            build=subprocess.check_output(cmd, stderr=DEVNULL, shell=True)
        except subprocess.CalledProcessError:
            build=None

        DEVNULL.close()
        if build==None:
            return -1
        build=build.decode("utf-8")
        build=build.replace("\n","").replace("\t","")
        #self.SetBuild(build)
        return 0
    def InitBoardMngr(self):        
        InitBoards(self.m_board_mngr)
    def FindCompiler(self, build):
        if self.GetBuild().GetCrossCompile()==False:
            self.SetCompilerBinPath=None
            return
        dirs=["/usr/local",
              ]
        for dir in dirs:
            path=dir+os.sep+"*"+build+"*"
            results=glob.glob(path)
            results.sort()
            if len(results)!=0:
                self.SetCompilerBinPath(results[-1]+os.sep+"bin")
    def WriteConfigFileInBuildDir(self):
        output_dir=self.GetOutputDir()
        output_dir=os.path.join(output_dir, ".esysbuild")
        if os.path.exists(output_dir)==False:
            os.makedirs(output_dir)
            
        if self.GetCfgFile()==None:
            print("ERROR: something")
            sys.exit(-1)
        assert self.GetBuildName()!=None, "ESysBuild.WriteConfigFileInBuildDir GetBuildName is None."
        assert self.GetBuild()!=None, "ESysBuild.WriteConfigFileInBuildDir GetBuild is None."
            
        cfg_file=self.GetCfgFile().CopyToBuildType(output_dir, self.GetBuild().GetFullName())
        cfg_file.Write(output_dir)
        
    def ConfigureCmd(self):
        if self.IsCMakeBuild()==True:            
            cmake=self.GetCMake()
            if self.IsWindow()==True:
                cmake.SetBuildDir(self.GetRepoRootDir()+"/build/cmake_win32")
            else:
                cmake.SetBuildDir(self.GetRepoRootDir()+"/build/cmake")
            cmake.SetRootDir(self.GetRepoRootDir())
            cmake.Configure()
            return
            
        build=self.GetBuild()
        if build==None:
            print("ERROR: build %s is unknown" % self.GetBuildName())
            sys.exit(1)
        result=self.LoadAllLibs()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the repo and libraries configuration")
            return result
        
        build_name=build.GetFullName()
        build_name=build_name.replace(".", os.sep)
        output_dir=os.path.join(self.GetBuildDir(),build_name)
        if os.path.exists(output_dir)==False:
            os.makedirs(output_dir)
        self.SetOutputDir(output_dir)
        
        self.m_fout=open(os.path.join(output_dir,"esysbuild_conf.txt"), "wt")
        
        self.WriteConfigFileInBuildDir()
        
        #self.FindCompiler(self.GetHost())

        #build=self.GetBuild()
        cwd=os.getcwd()
        os.chdir(output_dir)
        
        
        if self.IsFolderSet()==False:
            self.ConfigureAll()
        else:
            self.ConfigureFolders()
        self.m_fout.close()
        os.chdir(cwd)
    def ConfigureFolders(self):
        repo_mngr=self.GetRepoMngr()
        for folder in self.GetFolders():
            repo=repo_mngr.Find(folder)
            if repo!=None:
                self.ConfigureRepo(repo)
            else:
                self.Verbose(0, "ERROR: no repo was found with name or location %s" % folder)
    def GetModListForBuild(self):
        build=self.GetBuild()
        if build==None:
            self.Verbose(0, "ERROR: build %s is unknown" % self.GetBuildName())
            return                
        build_name_list=build.GetBuildNameList()
        build_list=[]
        build_dict={}
        for build_name in build_name_list:
            build_mod=self.GetLibMngr().Find(build_name)
            if build_mod==None:
                #TBD
                continue
            if build_name in build_dict:
                continue
            build_dict[build_name]=build_mod
            build_list.append(build_mod)
        return build_list
    def GetRepoListFromBuild(self, with_dependencies=True):
        if with_dependencies==True:
            build_dict, build_list=self.GetLibsToBuild()
        else: 
            build_list=self.GetModListForBuild()
        
        repo_dict={}
        repo_list=[]
        for mod in build_list:
            repo=mod.GetRepo()
            if repo.GetName() in repo_dict:
                continue
            repo_dict[repo.GetName()]=repo
            repo_list.append(repo)
        return repo_list
    def ConfigureAll(self):
        repo_list=self.GetRepoListFromBuild()
        build=self.GetBuild()
        #repo_mngr=self.GetRepoMngr()        
        #count=repo_mngr.GetCount()
        count=len(repo_list)
        if count==0:
            self.Verbose(0, "No repository found to configure.")
            return 0        
        for idx in range(count):
            #repo=repo_mngr.Get(idx)
            repo=repo_list[idx]
            configure_it=True
            if len(build.GetGroupList())!=0:
                repo_group_names=repo.GetGroupList()
                result=build.Intersection(repo_group_names)
                if len(result)==0:
                    configure_it=False
            if configure_it==True:
                self.ConfigureRepo(repo)
        return 0
    def ConfigureRepo(self, repo):
        self.Verbose(0, "Repo %s" % repo.GetLocation())
        self.Verbose(0, "Configure ...")
        output_dir=self.GetOutputDir()
        repo_out_dir=os.path.join(output_dir, repo.GetLocation())
        if os.path.exists(repo_out_dir)==False:
            os.makedirs(repo_out_dir)
        cwd=os.getcwd()
        os.chdir(repo_out_dir)
        configure_path=repo.GetDir()
        configure_rel_path=os.path.relpath(configure_path, repo_out_dir)
        cmd=configure_rel_path+os.sep+"configure"
        build=self.GetBuild()
        
        if build.GetCrossCompile()==True:
            cmd+=" CROSS_COMPILE=1 --host=%s --build=%s CC=%s CXX=%s" % (self.GetHost(), self.GetBuild(), self.GetCC(True), self.GetCXX(True))
        else:
            cmd+=" --enable-mysystemc --disable-nfc"
        cmd+=" --with-esysbuild"
        cmd+=" --with-board=%s" % self.GetBuildName()
        cmd+=" ESYS_DEV=%s" % self.GetRepoRootDir()
        cmd+=" ESYSCFG=%s/config" % self.GetRepoRootDir()
        cmd+=" ESYS_BUILD_DIR=%s" % output_dir
        esys_dir=os.path.join(self.GetRepoRootDir(),"src","esys")
        cmd+=" ESYS_DIR=%s" % esys_dir
        cmd+=" ESYS_MYSYSC_INC=%s" % os.path.join(self.GetRepoRootDir(),"extlib","mysystemc-231", "src")
        cmd+=" ESYS_DBG_LOG_INC=%s" % os.path.join(self.GetRepoRootDir(),"extlib","dbg_log", "include")
        cmd+=" ESYS_LOGMOD_LIB_DIR=%s" % os.path.join(output_dir, "extlib/dbg_log/src/logmod")
        cmd+=" ESYS_DBG_LOG_LIB_DIR=%s" % os.path.join(output_dir, "extlib/dbg_log/src/dbg_log")
        
        var_list=self.GetBuildFile().GetEnvVarRelDirList()
        for var in var_list:
            p=os.path.join(self.GetRepoRootDir(),var[1])
            cmd+=" %s=%s" % (var[0], p)

        if build.IsShared()==False:
            cmd+=" --disable-shared"
        if build.IsStatic()==False:
            cmd+=" --disable-static"
        
        if build.GetCFlags()!=None:
            cmd+=" CFLAGS=\"%s\"" % build.GetCFlags()
        if build.GetCXXFlags()!=None:
            cmd+=" CXXFLAGS=\"%s\"" % build.GetCXXFlags()
        if self.GetVerbose()==0:
            cmd+=" --quiet"
        #print "cmd = %s" % cmd
                
        result=self.CallCmd(cmd)
        
        os.chdir(cwd)        
        self.Verbose(0, "Configure done.\n")        
        return result
    def Do(self):
        self.ConfigureCmd()
    def CallCmd(self, cmd, shell=True):
        try:
            result=subprocess.call(cmd, shell=shell)
        except subprocess.CalledProcessError:
            result=None
        return result
    def CallCmdInFolders(self, cmd, shell=True):
        folders=self.GetFolders()
        if folders==None or len(folders)==0:
            result=self.CallCmd(cmd, shell)
        else:
            cwd=os.getcwd()
            for folder in folders:
                os.chdir(folder)
                result=self.CallCmd(cmd, shell)
                os.chdir(cwd)
    def BootStrapCmd(self):
        result=self.LoadAllLibs()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the repo and libraries configuration")
            return result
        if self.IsFolderSet()==False:
            self.BootStrapAllCmd()
        else:
            self.BootStrapFoldersCmd()
    def BootStrapFoldersCmd(self):
        repo_mngr=self.GetRepoMngr()
        for folder in self.GetFolders():
            repo=repo_mngr.Find(folder)
            if repo!=None:
                self.BootStrapRepo(repo)
            else:
                self.Verbose(0, "ERROR: no repo was found with name or location %s" % folder)
    def BootStrapRepo(self, repo):
        self.Verbose(0, "Repo %s" % repo.GetLocation())
        self.Verbose(0, "Bootstrap ...")
        build_file=repo.GetBuildFile()
        if build_file==None:
            self.Verbose(0, "ERROR: repo %s doesn't have a build file" % repo.GetName())
            return
        boot_strap_script=build_file.GetBootStrapScript()
        if boot_strap_script==None:
            boot_strap_script="config/bootstrap"

        path=repo.GetDir()
        cwd=os.getcwd()
        os.chdir(path)
        cmd=boot_strap_script
        self.CallCmd(cmd)
        self.Verbose(0, "Bootstrap done.\n")
        os.chdir(cwd)
    def BootStrapAllCmd(self):        
        repo_mngr=self.GetRepoMngr()        
        count=repo_mngr.GetCount()
        if count==0:
            self.Verbose(0, "No repository found.")
            return 0        
        for idx in range(count):
            repo=repo_mngr.Get(idx)
            self.BootStrapRepo(repo)
        return 0
        

    def MakeCmd(self, make_arg):
        build=self.GetBuild()
        if build==None:
            print("ERROR: build %s is unknown" % self.GetBuildName())
            sys.exit(1)
        
        build_name=build.GetFullName().replace(".", os.sep)
        output_dir=os.path.join(self.GetBuildDir(),build_name)
        if os.path.exists(output_dir)==False:
            os.makedirs(output_dir)
        self.SetOutputDir(output_dir)
                
        cmd="make"
        if make_arg!="" and make_arg!=None:
            #os.chdir(make_dir)
            cmd+=" %s" % make_arg
            if make_arg=="registermake":
                self.m_fout=open("esysbuild.txt", "wt")
                self.m_fout.write("EsysBuild results")
        if self.GetJobCount()!=None:
            cmd+=" -j%s" % self.GetJobCount()
                
        batch_list=self.GetResolvedRepoDependencyList()
        
        cwd=os.getcwd()
        
        for batch in batch_list:
            for repo in batch:
                output_dir=self.GetOutputDir()
                repo_output_dir=os.path.join(output_dir, repo.GetLocation())
                
                if os.path.exists(repo_output_dir)==False:
                    os.makedirs(repo_output_dir)
                
                os.chdir(repo_output_dir)
                result=self.CallCmd(cmd)
            
        os.chdir(cwd)            
        return result
    def WriteFile(self, s):
        if self.m_fout==None:
            self.m_fout=open(self.GetConfigureRoot()+os.sep+"esysbuild.txt", "at")
        self.m_fout.write(s)
        self.m_fout.write("\n")
    def RegConfCmd(self, reg_conf):
        s="reg_conf = %s" % reg_conf
        self.Verbose(1, s)
        self.WriteFile(s)
    def RegMakefileCmd(self, reg_makefile):
        print_it=True
        if type(reg_makefile)==list:
            if len(reg_makefile)==1 and reg_makefile[0]=='':
                print_it=False
        if print_it:
            s="reg_makefile = %s" % reg_makefile
            self.Verbose(1, s)
            self.WriteFile(s)
    def RegMakefileFailedCmd(self, reg_makefile_failed):
        s="reg_makefile_failed = %s" % reg_makefile_failed
        self.Verbose(1, s)
        self.WriteFile(s)
    def RegLibCmd(self, reg_lib):
        s="reg_lib = %s" % reg_lib
        self.Verbose(1, s)
        self.WriteFile(s)
    def CopyToBin(self, src_file, dst_file, name, exe=True):
        self.LogCall(2)
        if os.path.isfile(src_file)==False:
            print("ERROR: file %s is missing" % src_file)
            sys.exit(1)

        if os.path.isfile(dst_file)==True:
            if filecmp.cmp(dst_file, src_file)==True:
                self.Verbose(0,"latest "+name+" already installed")
                return 0
            else:
                self.Verbose(0,"Updating "+name+" to latest version ...")
        else:
            self.Verbose(0,"Installing "+name+" ...")

        try:
            shutil.copy2(src_file, dst_file)
        except IOError:
            self.Verbose(0,"ERROR: copying %s to %s failed" % (src_file, dst_file))
            sys.exit(1)
        if exe==True:
            if self.IsCygwin()==True:
                cmd="setfacl -s u::rwx,g::r-x,o:r-x %s" % dst_file
                result=subprocess.call(cmd, shell=True)
                #print "result chmod = %s" % result
            else:
                os.chmod(dst_file,stat.S_IRWXU+stat.S_IRWXG+stat.S_IRWXO)
        self.Verbose(0, name+" installed.")
        return 1
    def MakeExecutable(self, path):
        if self.IsCygwin()==True:
            cmd="setfacl -s u::rwx,g::r-x,o:r-x %s" % path
            result=subprocess.call(cmd, shell=True)
            if result!=0:
                self.Verbose(0,"ERROR: couldn't make file %s executable" % path)
        else:
            os.chmod(path,stat.S_IRWXU+stat.S_IRWXG+stat.S_IRWXO)
    def FindExe(self, name):
        """Try to find where the an executable is located

        Returns:
            True if found, False otherwise
        """
        found_exe=True
        DEVNULL = open(os.devnull, 'wb')
        try:
            path=subprocess.check_output("which "+name,  stderr=DEVNULL, shell=True)
        except subprocess.CalledProcessError:
            found_exe=False
            self.Verbose(1, "Couldn't find the %s script" % name)
        DEVNULL.close()
        if found_exe==True:
            path=path.decode("utf-8")
            path=path.replace("\n","")
            path=os.path.split(path)[0]
            self.Verbose(1, "Found the %s script in %s" % (name, path))
            return path
        return None
    def FindFolderUp(self, name, file_in_folder=None, strip= False, start_dir=None):
        """Look for a folder of a given name starting at the current directory or from a staring
        directory and going up

        """
        if start_dir==None:
            curdir = os.getcwd()
        else:
            curdir=start_dir
        folder = None
        found=False
        olddir = None
        while curdir != '/' \
            and curdir != olddir \
            and not folder:
            if file_in_folder!=None:
                folder = os.path.join(curdir, name, file_in_folder)
            else:
                folder = os.path.join(curdir, name)
            if not os.path.exists(folder):
                folder = None
                olddir = curdir
                curdir = os.path.dirname(curdir)
        #self.m_repo_root_dir=curdir
        if folder!=None:
            if file_in_folder!=None:
                folder=folder.replace(file_in_folder, "")
            if strip==True:
                folder=os.path.split(folder)[0]
            self.Verbose(1, "Found %s at %s" % (name,folder))
            return folder
        else:
            self.Verbose(1, "Couldn't find the %s" % name)
            return None
    def InstallESysBuildFolder(self, install_path):
        self.LogCall(2)
        if install_path.rfind(".esysbuild")==-1:
            path=os.path.join(install_path,".esysbuild")
        else:
            path=install_path
        if os.path.exists(path):
            # This is an update, but config.py file could be missing
            pass
        else:
            # This is a new installation
            os.mkdir(path)
        cfg_path=os.path.join(path, "config.py")
        if os.path.exists(cfg_path)==False or self.GetCfgFile()==None:
                        
            cfg_file=CfgFile(CfgFile.ROOT)
            #cfg_file.SetFolderPath(path)
            src_dir_rel=os.path.relpath(self.GetSrcDir(), path)
            cfg_file.SetESysBuildSrcDir(src_dir_rel)
            cfg_file.SetInDevFolder(self.GetInDevFolder())
            cfg_file.Write(path)
            
            #fout=open(os.path.join(path, "config.py"), "wt")
            #fout.write("type=\"root\"\n")
            #if self.GetInDevFolder()==False:
            #    fout.write("in_dev_folder=False\n")
            #else:
            #    fout.write("in_dev_folder=True\n")
            #fout.close()
        else:
            pass
    def SetInDevFolder(self, in_dev_folder):
        self.m_in_dev_folder=in_dev_folder
    def GetInDevFolder(self):
        return self.m_in_dev_folder
    def Init(self):
        assert self.GetScriptPath()!=None, "ESysBuild.Init assumes SetScriptPath was called"
        # Now check if there is a .repo and .git in a folder up from the current working directory
        repo_path=self.FindFolderUp(".repo", strip=True)
        git_repo_path=self.FindFolderUp(".git", strip=True)
        
        if repo_path==None and git_repo_path==None:
            self.Verbose(0,"ERROR: couldn't find a .repo or .git upwards, so assuming not in SW development tree.")
            return -1
        self.SetRepoRootDir(repo_path)
        if repo_path!=None:
            self.SetBuildDir(os.path.join(repo_path, "build"))
        else:
            self.SetBuildDir(os.path.join(git_repo_path, "build"))        
        # Search first for the .esysbuild folder, so we can load a config.py
        esysbuildcfg_path=self.FindFolderUp(".esysbuild")
        if esysbuildcfg_path!=None:
            # Found a .esysbuild folder
            self.SetConfigureRoot(esysbuildcfg_path)
            # The default location where the ESysBuild source should be found
            self.SetSrcDir(os.path.join(esysbuildcfg_path, "esysbuild"))
            # Load the configuration file
            self.LoadCfgFile()
            
        # Try to find if the ESysBuild is run from the source code
        script_folder=self.GetScriptFolder()
                
        git_repo_path=self.FindFolderUp(".git", strip=True, start_dir=script_folder)
        repo_path=self.FindFolderUp(".repo", strip=True, start_dir=script_folder)
        if git_repo_path==None and repo_path==None:
            # if no .repo or .git folder is found in a folder above the script, the script is not from source dev folder 
            self.SetInDevFolder(False)
        else:
            self.SetInDevFolder(True)
            self.SetSrcDir(script_folder)
            
        # Adds the python path to the location of ESysBuild source 
        sys.path.append(self.GetSrcDir())
        
        if self.GetBuildName()!=None:
            build_name=self.GetBuildName().replace(".", os.sep)
            output_dir=os.path.join(self.GetBuildDir(), build_name)
            if os.path.exists(output_dir)==False:
                os.makedirs(output_dir)
            self.SetOutputDir(output_dir)            
        return 0          
    def InstallScript(self):
        path=self.FindExe("esysbuild")
        if path==None:
            # Not found, let's try to find where repo script is installed
            path=self.FindExe("repo")
            if path==None:
                home_path=os.path.expanduser("~")
                if home_path==None:
                    self.Verbose(0,"ERROR: Can't find a user folder where to install esysbuild script")
                    sys.exit(1)
                path=os.path.join(home_path, "bin")
                if os.path.isdir(path)==False:
                    self.Verbose(0,"Warning: creating folder %s to store esysbuild script" % path)
                    os.makedirs(path)
                if (path in os.environ["PATH"].split(os.pathsep))==False:
                    self.Verbose(0,"Warning: esysbuild is copied in %s, but it's not in the PATH" % path)
            self.CopyToBin(os.path.join(self.GetSrcDir(),"main.py"), os.path.join(path,"esysbuild") , "esysbuild", True)            
        else:
            # If found, let's make sure it's the newest version
            if self.GetScriptFolder()!=path:
                self.CopyToBin(os.path.join(self.GetSrcDir(),"main.py"), os.path.join(path,"esysbuild"), "esysbuild", True)
        return path
    def InitCmd(self):
        cwd=os.getcwd()
        self.Verbose(2,"cwd = %s" % cwd)
        # First install the esysbuild script in a bin search path, by default same folder as repo script
        path=self.InstallScript()        
                        
        # At this point, esysbuild script should be found from any folder
        self.SetScriptPath(os.path.join(path, "esysbuild"))
        esysbuild_path=self.FindFolderUp(".esysbuild")
        if esysbuild_path==None:
            self.Verbose(2, ".esysbuild not found")
            repo_path=self.FindFolderUp(".repo")
            if repo_path==None:
                self.Verbose(2, ".repo not found")
                # This would mean that esysbuild is used in a git repo, but google repo tool is not used
                install_path=cwd
            else:
                install_path=os.path.split(repo_path)[0]
        else:
            if self.GetCfgFile()==None:
                self.Verbose(0,"Warning: found a .esysbuild folder at %s." % esysbuild_path)
                self.Verbose(0,"    But since loading the config file failed, we will overwrite it.\n")
                install_path=os.path.split(esysbuild_path)[0]
            else:
                self.Verbose(0,"Warning: found a .esysbuild folder at %s content, it won't be initialized again.\n" % esysbuild_path)
                return
        self.InstallESysBuildFolder(install_path)
    def LoadCfgFile(self):
        if self.m_cfg_file!=None:
            return 0
                
        cfg_path=os.path.join(self.GetConfigureRoot(), "config.py")
        result, cfg_file = CfgFile.Load(cfg_path)
        self.SetCfgFileLoadResult(result)
        self.SetCfgFile(cfg_file)
        if result<0:
            self.Verbose(0,"WARNING: couldn't load the config file found %s" % self.GetConfigureRoot())
            return result
            
        #spec = importlib.util.spec_from_file_location("config", cfg_path)
        #self.m_cfg_file = importlib.util.module_from_spec(spec)
        #spec.loader.exec_module(self.m_cfg_file)
        sys.modules["config"] =self.m_cfg_file
        if cfg_file.GetType()==CfgFile.BUILD:
            self.SetVerboseOff()
            
        if self.m_cfg_file.GetInDevFolder()==True:
            if self.m_cfg_file.GetESysBuildSrcDir()!=None:
                path=os.path.join(self.GetConfigureRoot(), os.path.normpath(self.m_cfg_file.GetESysBuildSrcDir()))
                path=os.path.abspath(path).replace("\\","/")
                sys.path.append(path)
                #print("path = %s" % path)
                self.SetSrcDir(path)
                self.InstallScript()
        if self.m_cfg_file.GetBuildName()!=None:            
            self.SetBuildName(self.m_cfg_file.GetBuildName())
                
        return 0
    def LoadBuildCfgFile(self):
        if self.m_build_cfg_file!=None:
            return 0
        cfg_path=os.path.join(self.GetRepoRootDir(), "esysbuild.cfg.py")
        repo_mngr=self.GetRepoMngr()
        repo_mngr.SetVerboseObj(self)
        lib_mngr=self.GetLibMngr()
        lib_mngr.SetVerboseObj(self)        
        repo_mngr.SetBaseDir(self.GetRepoRootDir())
        repo_mngr.SetLibMngr(lib_mngr)
        result, build_cfg_file=BuildFile.Load(cfg_path)
        
        if result<0:
            return result
        #spec = importlib.util.spec_from_file_location("esysbuild.cfg.py", cfg_path)
        #self.m_build_cfg_file = importlib.util.module_from_spec(spec)
        #spec.loader.exec_module(self.m_build_cfg_file)
        self.m_build_cfg_file=build_cfg_file
        mod=self.m_build_cfg_file.GetModule()
        
        if (mod!=None) and ("load_repo_cfg" in mod.__dict__):
            mod.load_repo_cfg(self.GetRepoMngr())
        self.SetBuildFile(build_cfg_file)
        build_cfg_file.AddGerrits(self.GetGerritMngr())
        #self.m_build_cfg_file.load_library_cfg(self.GetLibMngr())
                
        return 0
    def ListBoardCmd(self):
        print("Boards:")
        mngr=self.GetBoardMngr()
        for idx in range(mngr.GetCount()):
            name=mngr.GetBoardName(idx)
            print("[%.2i] %s" %(idx, name))
    def LoadAllLibs(self):
        result=self.LoadBuildCfgFile()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the list of libraries")
            return result
        repo_mngr=self.GetRepoMngr()
#         repo_mngr.SetVerboseObj(self)
#         lib_mngr=self.GetLibMngr()
#         lib_mngr.SetVerboseObj(self)        
#         repo_mngr.SetBaseDir(self.GetRepoRootDir())
#         repo_mngr.SetLibMngr(lib_mngr)        
        
        repo_mngr.LoadAllLibs()
        return 0
    def GetESysBuildSDK(self):
        if self.m_esysbuildsdk!=None:
            return self.m_esysbuildsdk
        esysbuildsdk=None
        if ("ESYSBUILDSDK" in os.environ)==True:
            esysbuildsdk=os.environ["ESYSBUILDSDK"]
        else:
            if self.IsLinux()==True:
                esysbuildsdk="/usr/local/lib/esysbuildsdk"
        if (esysbuildsdk==None) or (os.path.exists(esysbuildsdk)==False):
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: ESysBuildSDK is not found")
        self.m_esysbuildsdk=esysbuildsdk
        return esysbuildsdk
    def LoadAllCompilers(self):
        cxx_mngr=self.GetCompilerMngr()
        cxx_mngr.SetVerboseObj(self)

        esysbuildsdk=self.GetESysBuildSDK()
        if esysbuildsdk==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: couldn't find the location of ESysBuildSDK")
            return 0
        base_dir=os.path.join(esysbuildsdk, "compilers")
        cxx_mngr.SetBaseDir(base_dir)
        result=cxx_mngr.LoadAll()                
        return result
    def ListCXXCmd(self):
        self.Verbose(0, "List of compilers:")
        result=self.LoadAllCompilers()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the list of compilers")
            return result
        cxx_mngr=self.GetCompilerMngr()
        cxx_list=cxx_mngr.GetList()
        if cxx_list==None or len(cxx_list)==0:
            self.Verbose(0, "No compiler found.")
            return 0
        for cxx in cxx_list:
            txt="%s %s" %(cxx.GetName(), cxx.GetVersion())
            self.Verbose(0, txt)
        return 0
    def LoadAllBuilds(self):
        result=self.LoadBuildCfgFile()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the main_old build file")
            return result
        build_mngr=self.GetBuildMngr()
        if self.GetBuildFile()!=None:
            build_mngr.AddSearchPath(self.GetBuildFile().GetBuildsDir())
            sys.path.append(self.GetBuildFile().GetBuildsDir())            
        build_mngr.SetVerboseObj(self)
        return build_mngr.LoadAll()        
    def ListBuildCmd(self):
        self.Verbose(0, "List of builds:")
        result=self.LoadAllBuilds()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the list of builds")
            return result
        build_mngr=self.GetBuildMngr()
        build_list=build_mngr.GetList()
        idx=0
        for build in build_list:
            txt=build.GetFullName()
            self.Verbose(0, "[%.2i] %s" % (idx,txt) )
            idx+=1
        return 0
    def ListLibCmd(self):
        result=self.LoadAllLibs()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the list of libraries")
            return result
        repo_mngr=self.GetRepoMngr()
        if True:
            count=repo_mngr.GetCount()
            if count==0:
                self.Verbose(0,"ERROR: No repository found.")
                return 0
            print("Repos and their libraries:")
            lib_idx=0
            for idx in range(count):
                repo=repo_mngr.Get(idx)
                print("[%.2i] %s" % (idx, repo.GetName()))
                for idx2 in range(repo.GetCount()):
                    lib=repo.Get(idx2)
                    print("  [%.2i] %s" % (lib_idx, lib.GetName()))
                    lib_idx+=1
            return 0
        else:
            lib_mngr=self.GetLibMngr()
            count=lib_mngr.GetCount()
            if count==0:
                self.Verbose(0,"No libraries found.")
                return 0
            print("Libraries list:")
            for idx in range(count):
                lib=lib_mngr.GetLib(idx)
                print("[%.2i] %s" % (idx, lib.GetName()))
            return 0
    def LibCmd(self, lib_name, args):
        result=self.LoadAllLibs()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the list of libraries")
            return result
        lib_mngr=self.GetLibMngr()
        lib=lib_mngr.Find(lib_name)
        if lib==None:
            return -1
        
        if args.ldflags!=None:
            return self.LibLDFlafsCmd(lib)
        elif args.lib_dir==True:
            return self.LibLibDirCmd(lib)
        elif args.inc_dir==True:
            return self.LibIncDirCmd(lib)        
        else:
            self.Verbose(0, "ERROR: must specify either --ldflags, --inc-dir, --lib-dir")
            return -1
    def LibIncDirCmd(self, lib):
        import esysbuild.repo
        
        repo=lib.GetRepo()
        assert repo!=None
        
        link_cfg=repo.GetLinkCfg()
        
        if link_cfg==esysbuild.repo.Repo.LINK_INSTALLED:
            if repo.GetPrefix()!=None:
                print(repo.GetPrefix())
                return 0
        incs=repo.GetIncPaths(link_cfg)
        if incs==None:
            
            return -1
        if len(incs)==0:
            return -2
        inc=incs[0]
        if link_cfg==esysbuild.repo.Repo.LINK_INSTALLED:
            pass
        elif link_cfg==esysbuild.repo.Repo.LINK_LIBTOOL:
            if self.GetBuildName()==None:
                self.SetVerboseOn()
                self.Verbose(0, "ERROR: a build must be specified by -b or --build or by calling esysbuild from within a build tree.")
                return -3
            inc=os.path.normpath(inc)  
            inc_dir=os.path.join(self.GetOutputDir(), repo.GetLocation(), inc)
            print(inc_dir)
        elif link_cfg==esysbuild.lib.Lib.LINK_PKG:
            pass
        return 0        
    def LibLibDirCmd(self, lib):
        import esysbuild.lib
        
        repo=lib.GetRepo()
        assert repo!=None
        
        link_cfg=repo.GetLinkCfg()
        if link_cfg==None:
            return -1
        if link_cfg==esysbuild.lib.Lib.LINK_INSTALLED:
            lib_dir=repo.GetPrefix()
        elif link_cfg==esysbuild.lib.Lib.LINK_LIBTOOL:
            if self.GetBuildName()==None:
                self.SetVerboseOn()
                self.Verbose(0, "ERROR: a build/board must be specified by -b or --board or by calling esysbuild from within a build tree.")
                return -3                  
            ldflags=lib.GetLinkCfg(esysbuild.lib.Lib.LINK_LIBTOOL) 
            ldflags=os.path.split(ldflags)[0]              
            lib_dir=os.path.join(self.GetOutputDir(), repo.GetLocation(), ldflags)
        elif link_cfg==esysbuild.lib.Lib.LINK_PKG:
            lib_dir=None #lib.GetLinkCfg(esysbuild.lib.Lib.LINK_PKG)
        else:
            return -3
        if lib_dir==None:
            self.Verbose(0, "ERROR: couldn't find the library directory for %s" % lib.GetName())
            return -4
        print(lib_dir)
        return 0
    def LibLDFlafsCmd(self, lib):
        import esysbuild.lib
        
        repo=lib.GetRepo()
        assert repo!=None
        if (self.GetLDFlags()==None) or (self.GetLDFlags()==""):            
            link_cfg=repo.GetLinkCfg()
        else:
            link_cfg=None
            ldflags=self.GetLDFlags()
            if ldflags!=None:            
                if self.GetLDFlags()=="inst":
                    link_cfg=esysbuild.lib.Lib.LINK_INSTALLED                    
                elif self.GetLDFlags()=="libtool":
                    link_cfg=esysbuild.lib.Lib.LINK_LIBTOOL                                        
                elif self.GetLDFlags()=="pkg":
                    link_cfg=esysbuild.lib.Lib.LINK_PKG                    
        if link_cfg==None:
            return -1
        if link_cfg==esysbuild.lib.Lib.LINK_INSTALLED:
            ldflags=lib.GetLinkCfg(esysbuild.lib.Lib.LINK_INSTALLED)
        elif link_cfg==esysbuild.lib.Lib.LINK_LIBTOOL:
            ldflags=lib.GetLinkCfg(esysbuild.lib.Lib.LINK_LIBTOOL)                        
            ldflags=os.path.join(self.GetOutputDir(), repo.GetLocation(), ldflags)
        elif link_cfg==esysbuild.lib.Lib.LINK_PKG:
            ldflags=lib.GetLinkCfg(esysbuild.lib.Lib.LINK_PKG)
        else:
            return -3
        print(ldflags)
        return 0
    def ListRepoCmd(self):
        result=self.LoadBuildCfgFile()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the list of libraries")
            return result
        repo_mngr=self.GetRepoMngr()
        count=repo_mngr.GetCount()
        if count==0:
            print( "No libraries found.")
            return 0
        print("Libraries list:")
        for idx in range(count):
            repo=repo_mngr.Get(idx)
            print("[%.2i] %s" % (idx, repo.GetName()))
        return 0
    def FindTopRootFolder(self):
        #Top root folder is the one next to .repo folder if any, otherwise next to .git if repo is not used
        pass
    def TopicCmd(self, args):
        result=self.LoadBuildCfgFile()
        if result<0:
            return result
        topics=[]
        if type(args.topic)==str:
            l=args.topic.split(",")
            topics+=l
        elif type(args.topic)==list:
            for item in args.topic:
                l=item.split(",")
                topics+=l
        gerrit_mngr=self.GetGerritMngr()
        changes=gerrit_mngr.GetChanges(topics, True)
        if args.show==True:
            self.ShowChanges(changes)
        elif args.download==True:
            self.DownloadChanges(changes, topics)
        return 0     
    def ShowChanges(self, changes):
        self.Verbose(0, "Show gerrit changes:")
        for change in changes:
            self.ShowChange(change)
            self.Verbose(0,"")
    def ShowChange(self, change):
        pyobj=change.GetPy()
        number=pyobj["number"]
        self.Verbose(0, "Change  = %s" % number)
        project=pyobj["project"]
        self.Verbose(0, "Project = %s" % project)
        subject=pyobj["subject"]
        self.Verbose(0, "Subject = %s" % subject)
        topic=pyobj["topic"]
        self.Verbose(0, "Topic   = %s" % topic)
        id=pyobj["id"]
        self.Verbose(0, "Id      = %s" % id)
        url=pyobj["url"]
        self.Verbose(0, "URL     = %s" % url)
        self.Verbose(0, "Patchsets:")
        patchsets=pyobj["patchSets"]
        for patchset in patchsets:
            number=patchset["number"]
            self.Verbose(0, "Number    = %s" % number)
            ref=patchset["ref"]
            self.Verbose(0, "Ref       = %s" % ref)
            uploader=patchset["uploader"]
            username=uploader["username"]
            self.Verbose(0, "Uploader  = %s" % username)
            author=patchset["author"]
            username=author["username"]
            self.Verbose(0, "Author    = %s" % username)            
    def DownloadChanges(self, changes, topics):
        result=self.LoadAllLibs()
        if result<0:
            self.Verbose(0, "ERROR: couldn't load the libraries.")
            return result
        self.Verbose(0, "Download changes for topics: %s" % topics)
        for change in changes:
            self.DownloadChange(change)
    def DownloadChange(self, change):
        pyobj=change.GetPy()
        change_number=pyobj["number"]
        project=pyobj["project"]
        
        patchsets=pyobj["patchSets"]
        
        sorted_patchets=list(reversed(sorted(patchsets, key=lambda value: int(value['number']))))
        patchset=sorted_patchets[0]
        patchset_number=patchset["number"]
        self.Verbose(0, "Download from project %s change %s patchset %s" % (project, change_number, patchset_number))
        
        cmd="repo download "
        #lib_mngr=self.GetLibMngr()
        #lib=lib_mngr.Find(project)
        #project_path=lib.GetLocation()
        cmd+=project+" "
        cmd+="%s/%s" % (change_number, patchset_number)
        
        self.Verbose(1,"cmd = \"%s\"" % cmd)
        if self.GetDryRun()==True:
            return 0
        DEVNULL = open(os.devnull, 'wb')        
        try:
            result=subprocess.check_output(cmd, stderr=DEVNULL, shell=True)
        except subprocess.CalledProcessError:
            result=None
        DEVNULL.close()
        
        pass 
        return 0
    def ListBuildTreeCmd(self):
        if self.GetBuildName()==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: a build/board must be specified by -b or --board or by calling esysbuild from within a build tree.")
            return -1
        result=self.LoadAllLibs()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the list of libraries")
            return result
        
        build=self.GetBuild()
        build_list=self.GetModListForBuild()
        for build_mod in build_list:
            self.ShowBuildTree(build_mod)
    def ShowBuildTree(self, build_mod, lvl=0):
        txt=""
        for idx in range(lvl):
            txt+="    "
        self.Verbose(0, txt+build_mod.GetName())
        for dep_name in build_mod.GetDependenciesList():
            dep=self.GetLibMngr().Find(dep_name)
            if dep==None:
                self.Verbose(0, "ERROR: dependency \"%s\" not found" % dep_name)
            else:
                self.ShowBuildTree(dep, lvl+1)
    def GetLibsToBuild(self):
        if self.GetBuildName()==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: a build/board must be specified by -b or --board or by calling esysbuild from within a build tree.")
            return -1
        result=self.LoadAllLibs()
        if result<0:
            self.Verbose(0,"ERROR: couldn't load the list of libraries")
            return result
        
        build=self.GetBuild()
        build_name_list=build.GetBuildNameList()
        build_list=[]
        build_dict={}
        for build_name in build_name_list:
            build_mod=self.GetLibMngr().Find(build_name)
            if build_mod==None:
                #TBD
                continue
            if build_name in build_dict:
                continue
            build_dict[build_name]=build_mod
            build_list.append(build_mod)
            depends=self.GetAllDependencies(build_mod)
            for depend in depends:
                if depend.GetName() in build_dict:
                    continue 
                build_dict[depend.GetName()]=depend
                build_list.append(depend)
        return [build_dict, build_list]
    def GetAllDependencies(self, mod):
        result=[]
        dep_names=mod.GetDependenciesList()
        for dep_name in dep_names:
            dep=self.GetLibMngr().Find(dep_name)
            if dep==None: 
                continue
            result.append(dep)
            result+=self.GetAllDependencies(dep)
        return result
    def GetResolvedLibDependencyList(self, all_libs=False, mod_list=None):
        if mod_list!=None:
            build_list=mod_list
        else:
            if all_libs==False:
                build_dict, build_list=self.GetLibsToBuild()
            else:
                if self.GetBuildName()==None:
                    self.SetVerboseOn()
                    self.Verbose(0, "ERROR: a build/board must be specified by -b or --board or by calling esysbuild from within a build tree.")
                    return -1
                result=self.LoadAllLibs()
                if result<0:
                    self.Verbose(0,"ERROR: couldn't load the list of libraries")
                    return result
                build_list=self.GetLibMngr().GetLibList()
        
        dict_libs=dict( (lib.GetName(), lib) for lib in build_list )
        dict_depends= dict( (lib.GetName(), set(lib.GetDependenciesList())) for lib in build_list )
        build_in_batches=[]
        
        while dict_depends:
            ready = {name for name, deps in list(dict_depends.items()) if not deps}
            if not ready:
                l=list(dict_depends.items())
                self.Verbose(0, "ERROR: circular dependency between %s" % l)
                return -1 
            for name in ready:
                del dict_depends[name]
            for deps in list(dict_depends.values()):
                deps.difference_update(ready)

            # Add the batch to the list
            build_in_batches.append( [name for name in ready] )
        return build_in_batches
    def GetResolvedRepoDependencyList(self, all_libs=False):
        repo_list=self.GetRepoListFromBuild()
        
        for repo in repo_list:
            repo_dep_dict={}
            repo_dep_list=[]
            repo_dep_name_list=[]
            lib_list=repo.GetLibList()
            for lib in lib_list:
                lib_dep_list=self.GetAllDependencies(lib)
                for lib_dep in lib_dep_list:
                    repo_dep=lib_dep.GetRepo()
                    if repo_dep.GetName() in repo_dep_dict:
                        continue
                    if repo.GetName()==repo_dep.GetName():
                        # Doesn't take into account dependencies between modules in same repo
                        continue
                    repo_dep_dict[repo_dep.GetName()]=repo_dep
                    repo_dep_list.append(repo_dep)
                    repo_dep_name_list.append(repo_dep.GetName())
            repo.AddDependencies(repo_dep_list)
                    
                
        dict_repos=dict( (repo.GetName(), repo) for repo in repo_list )            
        dict_depends= dict( (repo.GetName(), set(repo_dep.GetName() for repo_dep in repo.GetDependenciesList())) for repo in repo_list )
        build_in_batches=[]
        
        while dict_depends:
            ready = {name for name, deps in list(dict_depends.items()) if not deps}
            if not ready:
                self.Verbose(0, "ERROR: circular dependency between")
                return -1 
            for name in ready:
                del dict_depends[name]
            for deps in list(dict_depends.values()):
                deps.difference_update(ready)

            # Add the batch to the list
            build_in_batches.append( [dict_repos[name] for name in ready] )
        return build_in_batches                    
    def BuildOrderCmd(self):
        self.Verbose(0, "List of concurrent build of libraries:")
        batch_buil_lib_list=self.GetResolvedLibDependencyList()
        idx=0
        lib_idx=0
        for batch in batch_buil_lib_list:
            self.Verbose(0, "Batch %.2i:" % idx)
            for name in batch:
                self.Verbose(0,"[%.2i] %s " %(lib_idx, name))
                lib_idx+=1
            idx+=1
    def BuildOrderRepoCmd(self):
        self.Verbose(0, "List of concurrent build of repos:")
        batch_build_repo_list=self.GetResolvedRepoDependencyList()
        idx=0
        repo_idx=0
        for batch in batch_build_repo_list:
            self.Verbose(0, "Batch %.2i:" % idx)
            for repo in batch:
                self.Verbose(0,"[%.2i] %s " %(repo_idx, repo.GetName()))
                repo_idx+=1
            idx+=1    
    def IsBuildLibCmd(self, lib_name):        
        build_dict, build_list=self.GetLibsToBuild()
        if lib_name in build_dict:
            print("yes")
        else:
            print("no")
    def IsBuildRepoCmd(self, repo_name):        
        build_dict, build_list=self.GetLibsToBuild()
        repo_dict={}
        for build_mod in build_list:
            repo=build_mod.GetRepo()
            if repo==None:
                #TBD
                continue    
            if repo.GetName() in repo_dict:
                continue
            repo_dict[repo.GetName()]=True
        if repo_name in repo_dict:
            print("yes")
        else:
            print("no")
    def IsCXXSetCmd(self):
        if self.GetBuildName()==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: a build/board must be specified by -b or --board or by calling esysbuild from within a build tree.")
            return -1            
        build=self.GetBuild()
        if build==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: the build \"%s\" is unknown" % self.GetBuildName())
            return -1
        if build.GetCompiler()!=None:
            print("yes")
        else:
            print("no")
    def GetCXXCmd(self, prog):
        if self.GetBuildName()==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: a build/board must be specified by -b or --board or by calling esysbuild from within a build tree.")
            return -1            
        build=self.GetBuild()
        cxx_name=build.GetCompiler()
        if cxx_name==None:
            cxx_name="default"            
        result=self.LoadAllCompilers()
        if result<0:
            self.VerboseOn()
            self.Verbose(0, "ERROR: couldn't load info about the compilers")
            return -1
        cxx=self.GetCompilerMngr().Find(cxx_name)
        if cxx==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: couldn't find the compiler")
            return -1
        path=cxx.GetProgPath(prog)
        if path==None:
            print("None")
        else:
            print(path)
    def IsArchCmd(self, arch):
        if self.GetBuildName()==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: a build must be specified by -b or --board or by calling esysbuild from within a build tree.")
            return -1            
        build=self.GetBuild()
        if build==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: the build \"%s\" is unknown" % self.GetBuildName())
            return -1
        build_arch=build.GetArch()
        if build_arch==arch:
            print("yes")
        else:
            print("no")            
    def IsBoardCmd(self, board):
        if self.GetBuildName()==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: a build must be specified by -b or --board or by calling esysbuild from within a build tree.")
            return -1            
        build=self.GetBuild()
        if build==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: the build \"%s\" is unknown" % self.GetBuildName())
            return -1
        board_name=build.GetBoardName()
        if board_name==board:
            print("yes")
        else:
            print("no")
    def SetCMakeBuild(self, cmake_build):
        self.m_cmake_build=cmake_build
    def IsCMakeBuild(self):
        return self.m_cmake_build
    def GetCMake(self):
        if self.m_cmake==None:
            import esysbuild.cmake
            self.m_cmake=esysbuild.cmake.CMake(self)
        return self.m_cmake
    def CreateEclipseCmd(self):
        if self.GetBuildName()==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: a build must be specified by -b or --board or by calling esysbuild from within a build tree.")
            return -1
        build=self.GetBuild()
        if build==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: the build \"%s\" is unknown" % self.GetBuildName())
            return -1        
        cxx_name=build.GetCompiler()
        if cxx_name==None:
            cxx_name="default"            
        result=self.LoadAllCompilers()
        if result<0:
            self.VerboseOn()
            self.Verbose(0, "ERROR: couldn't load info about the compilers")
            return -1
        cxx_ver=self.GetCxxVer()
        cxx=self.GetCompilerMngr().Find(cxx_name, cxx_ver)
        if cxx==None:
            self.SetVerboseOn()
            self.Verbose(0, "ERROR: couldn't find the compiler")
            return -1
        sub_dir=build.GetFullName()
        sub_dir+=os.sep+cxx.GetName()+os.sep+cxx.GetVersion()
        if self.GetFolders()!=None and self.GetFolders()!=[]:
            if len(build.GetFullName())>1:
                self.Verbose(0, "WARNING: only the first folder given will be used") 
            folder=self.GetFolders()[0]
            folder+=os.sep+sub_dir
        else:
            repo_root_dir=self.GetRepoRootDir()
            dirs=os.path.split(repo_root_dir)
            folder=os.path.join(dirs[0], dirs[1]+"_build", sub_dir)            
        if self.GetDebugBuild()==True:
            folder=os.path.join(folder, "debug")
        else:
            folder=os.path.join(folder, "release")            
        folder=os.path.abspath(folder)
        #os.makedirs(folder)
        cmake=self.GetCMake()
        cmake.SetBuildDir(folder)
        cmake.SetRootDir(self.GetRepoRootDir())
        return cmake.CreateEclipse(folder, build, cxx)
            
def NewCXXAction(name):            
    class CXXAction(argparse.Action):
        def __init__(self, help=("return %s path" % name), option_strings=None, dest="get_cxx", default=None, const=None, nargs=0, **kwargs):                    
            super().__init__(option_strings=option_strings, const=const, help=help, dest=dest, nargs=nargs, default=default, **kwargs)
                        
        def __call__(self, parser, namespace, values, option_string=None):
            value=option_string.replace("--","")
            setattr(namespace, "get_cxx", value)
    return CXXAction
    


def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("folders", default=None, nargs='*', help="list of folders where to run command" )
    parser.add_argument("--init", dest="init", action="store_true", default=False, help="initialize a repo" )
    parser.add_argument("--bootstrap", dest="bootstrap", action="store_true", default=False, help="calls bootstrap" )
    parser.add_argument("--configure", dest="configure", action="store_true", default=False, help="calls configure" )
    parser.add_argument("--make-target", dest="make_target", nargs='?', default=None, const="", help="calls make" )
    parser.add_argument("--make", dest="make", action="store_true", default=False, help="calls make" )
    parser.add_argument("-l", "--list-board", dest="list_board",  action="store_true", default=False, help="list supported boards")
    parser.add_argument("--board", dest="board",  default=None, help="the board to build for")
    parser.add_argument("--list-repo", dest="list_repo",  action="store_true", default=False, help="list repositories in the build")
    parser.add_argument("--list-lib", dest="list_lib",  action="store_true", default=False, help="list libraries in the build")
    parser.add_argument("--lib", dest="lib",  default=None, help="the target lib")
    parser.add_argument("--lib-dir", dest="lib_dir", action="store_true", default=False, help="the target lib directory")
    parser.add_argument("--inc-dir", dest="inc_dir",  action="store_true", default=False, help="the target lib include directory")
    parser.add_argument("--ldflags", dest="ldflags", nargs='?', default=None, const="", help="give linker flags for a library")
    parser.add_argument("-b", "--build", dest="build",  default=None, help="set the build")
    parser.add_argument("-v", "--verbose", dest="verbose", nargs='?', type=int, default=None, const=1, help="verbose level")
    parser.add_argument("-j", "--job", dest="job", type=int, default=None, help="number of jobs")
    #parser.add_argument("--reg-conf", dest="reg_conf", action="store_true", default=False, help="register a configure script" )
    parser.add_argument("--reg-conf", dest="reg_conf", nargs='+', type=str, default=None, help="register a configure script" )
    parser.add_argument("--reg-makefile", dest="reg_makefile", nargs='+', type=str, default=None, help="register a makefile" )
    parser.add_argument("--reg-makefile-failed", dest="reg_makefile_failed", nargs='+', type=str, default=None, help="register a makefile failed" )
    parser.add_argument("--reg-lib", dest="reg_lib", nargs='+', type=str, default=None, help="register a library" )
    parser.add_argument("--cxxflags", dest="cxxflags", nargs='+', type=str, default=None, help="returns cxxflags " )
    parser.add_argument("--cflags", dest="cflags", nargs='+', type=str, default=None, help="returns cflags " )
    parser.add_argument("--cppflags", dest="cppflags", nargs='+', type=str, default=None, help="returns cppflags " )
    parser.add_argument("--libs", dest="libs", nargs='+', type=str, default=None, help="returns libs " )
    parser.add_argument("--list-cxx", dest="list_cxx",  action="store_true", default=False, help="list known compilers" )
    parser.add_argument("--list-build", dest="list_build",  action="store_true", default=False, help="list builds" )
    parser.add_argument("--topic", dest="topic", nargs='+', type=str, default=None, help="the topics of interest " )
    parser.add_argument("--show", dest="show", action="store_true", default=False, help="show the required info" )
    parser.add_argument("--download", dest="download", action="store_true", default=False, help="donwload action" )
    parser.add_argument("--dry-run", dest="dry_run", action="store_true", default=False, help="dry run" )
    parser.add_argument("--build-list", dest="build_list", action="store_true", default=False, help="list what's build" )
    parser.add_argument("--build-order", dest="build_order", action="store_true", default=False, help="list the build order" )
    parser.add_argument("--build-order-repo", dest="build_order_repo", action="store_true", default=False, help="list the repo build order" )
    parser.add_argument("--build-tree", dest="build_tree", action="store_true", default=False, help="show the build tree" )
    parser.add_argument("--is-build-repo", dest="is_build_repo", default=None, help="return yes if the repo is part of the build" )
    parser.add_argument("--is-build-lib", dest="is_build_lib", default=None, help="return yes if the lib part of the build" )
    parser.add_argument("--is-cxx-set", dest="is_cxx_set", action="store_true", default=False, help="return yes if compiler was set" )
    parser.add_argument("--set-cxx-ver", dest="set_cxx_ver", type=str, default=None, help="version of the compiler to use" )
    parser.add_argument("--cxx", dest="get_cxx", default=None, action=NewCXXAction("C++ compiler") )
    parser.add_argument("--cc", action=NewCXXAction("C compiler") )
    parser.add_argument("--ld", action=NewCXXAction("linker") )
    parser.add_argument("--ar", action=NewCXXAction("archive") )
    parser.add_argument("--objdump", action=NewCXXAction("object dump") )
    parser.add_argument("--nm", action=NewCXXAction("nm") )
    parser.add_argument("--ranlib", action=NewCXXAction("ranlib") )
    parser.add_argument("--objcopy", action=NewCXXAction("objcopy") )
    parser.add_argument("--size", action=NewCXXAction("size") )
    parser.add_argument("--is-arch", dest="is_arch",  default=None, help="return yes if the architecture is the correct")
    parser.add_argument("--is-board", dest="is_board",  default=None, help="return yes if the board is the correct")
    
    parser.add_argument("--cmake", dest="cmake", action="store_true", default=False, help="CMake build")
    parser.add_argument("--create-eclipse", dest="create_eclipse", action="store_true", default=False, help="build Eclipse project files")
    parser.add_argument("--toolchain", dest="toolchain",  default=None, help="set the toolchain, e.g. gnu, iar, arm")
    
    parser.add_argument("--debug", dest="debug", action="store_true", default=False, help="Debug build (default)")
    parser.add_argument("--release", dest="release", action="store_true", default=False, help="Release build")
        
    args = parser.parse_args()

    esys_build=ESysBuild()
    
    if (args.lib!=None) and (args.verbose==None):
        esys_build.SetVerboseOff()
    if args.verbose!=None:
        esys_build.SetVerbose(args.verbose)
    
    esys_build.SetScriptPath(script_path)
    esys_build.Init()

    import esysbuild.libmngr
    import esysbuild.lib
    #configure_root=os.path.abspath(globals()["__file__"])
    #configure_root=os.path.split(configure_root)[0]
    #print "configure_root = %s" % configure_root

    #esys_build.SetConfigureRoot(configure_root)
    
    if args.init==True:
        esys_build.InitCmd()
        sys.exit(0)

    if args.cmake==True:
        esys_build.SetCMakeBuild(True)

    if esys_build.GetConfigureRoot()==None:
        print("Error: can't find an ESysBuild folder. Please run esysbuild --init first.")
        sys.exit(1)

    esys_build.InitBoardMngr()
    
    if args.build!=None:
        esys_build.SetBuildName(args.build, True)
        
    if args.list_cxx==True:
        esys_build.ListCXXCmd()
        return
    elif args.list_build==True:
        esys_build.ListBuildCmd()
        return
    elif args.list_board==True:
        esys_build.ListBoardCmd()
        return
                
    if args.build==None:
        result=esys_build.DetectBuild()
        if result!=0:
            print("ERROR: couldn't detect the build, you must provide it with --build")
            sys.exit(1)
    if args.verbose!=None:
        esys_build.SetVerbose(args.verbose)
    if args.dry_run==True:
        esys_build.SetDryRun()
    if args.job!=None:
        esys_build.SetJobCount(args.job)
    if args.set_cxx_ver!=None:
        esys_build.SetCxxVer(args.set_cxx_ver)
    if args.release==True and args.release==True:
        print("ERROR: can't be both a Debug and a Release build, only one of --release or --debug must be used")
        sys.exit(1)
    elif args.release==None or args.release==False:
        esys_build.SetDebugBuild()
    elif args.release==True:
        esys_build.SetReleaseBuild()
    else:
        esys_build.SetDebugBuild()
        
    #print args
    esys_build.SetFolders(args.folders)
        
    if args.list_lib==True:
        esys_build.ListLibCmd()
    elif args.is_arch!=None:
        esys_build.IsArchCmd(args.is_arch)
    elif args.is_board!=None:
        esys_build.IsBoardCmd(args.is_board)
    elif args.get_cxx:
        esys_build.GetCXXCmd(args.get_cxx)
    elif args.is_cxx_set==True:
        esys_build.IsCXXSetCmd()
    elif args.is_build_repo!=None:
        esys_build.IsBuildRepoCmd(args.is_build_repo)
    elif args.is_build_lib!=None:
        esys_build.IsBuildLibCmd(args.is_build_lib)
    elif args.build_tree==True:
        esys_build.ListBuildTreeCmd()
    elif args.build_order==True:
        esys_build.BuildOrderCmd()
    elif args.build_order_repo==True:
        esys_build.BuildOrderRepoCmd()
    elif args.topic!=None:
        esys_build.TopicCmd(args)        
    elif args.lib!=None:
        if args.ldflags!=None:
            esys_build.SetLDFlags(args.ldflags)
        esys_build.LibCmd(args.lib, args)
    elif args.list_repo==True:
        esys_build.ListRepoCmd()
    elif args.bootstrap==True:
        esys_build.BootStrapCmd()
    elif args.configure==True:
        esys_build.ConfigureCmd()
    elif args.make_target!=None:
        esys_build.MakeCmd(args.make_target)
    elif args.make==True:
        esys_build.MakeCmd(None)    
    elif args.reg_conf!=None:
        esys_build.RegConfCmd(args.reg_conf)
    elif args.reg_makefile!=None:
        esys_build.RegMakefileCmd(args.reg_makefile)
    elif args.reg_makefile_failed!=None:
        esys_build.RegMakefileFailedCmd(args.reg_makefile_failed)
    elif args.create_eclipse==True:
        esys_build.CreateEclipseCmd()
    elif args.reg_lib!=None:
        esys_build.RegLibCmd(args.reg_lib)
    else:
        print("ERROR: nothing to do")
        sys.exit(1)
    esys_build.Close()

if __name__ == "__main__":
    main()

